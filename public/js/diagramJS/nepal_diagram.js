
//		26 494 504

var ctx = document.getElementById("graphOfPopulationMaleVSFemale");
ctx.height = 200;
var piechartPopuationMaleVSFemale = new Chart(ctx,{
    type: 'pie',
    data :{
        labels: [
            "Population of Male",
            "Population of Female",
        ],
        datasets: [
            {
                data: [12849041, 13645463],
                backgroundColor: [
                    dynamicColors(),
                    dynamicColors()
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

var ctx = document.getElementById("graphOfPopulationAgeGroup");
ctx.height = 200;
var piechartPopuationAgeGroup = new Chart(ctx,{
    type: 'pie',
    data :{
        labels: [
            "Population of 0-14 Age",
            "Population of 15-64 Age",
            "Population of 65+ Age",
        ],
        datasets: [
            {
                data: [9248246, 15848675, 1397583],
                backgroundColor: [
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors()
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

var ctx = document.getElementById("graphOfProvincePopulation");
ctx.height = 200;
var piechartPopuationAgeGroup = new Chart(ctx,{
    type: 'pie',
    data :{
        labels: [
            "Population of Province 1",
            "Population of Province 2",
            "Population of Province 3",
            "Population of Province 4",
            "Population of Province 5",
            "Population of Province 6",
            "Population of Province 7",

        ],
        datasets: [
            {
                data: [4534943, 5404145, 5529452, 2413907, 4891025, 1168515, 2552517],
                backgroundColor: [
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});


var ctx = document.getElementById("graphOfProvinceArea");
ctx.height = 200;
var piechartPopuationAgeGroup = new Chart(ctx,{
    type: 'pie',
    data :{
        labels: [
            "Area of Province 1",
            "Area of Province 2",
            "Area of Province 3",
            "Area of Province 4",
            "Area of Province 5",
            "Area of Province 6",
            "Area of Province 7",

        ],
        datasets: [
            {
                data: [25905, 9661, 20300, 21504, 22288, 27984 ,19539],
                backgroundColor: [
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                    dynamicColors(),
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};