var base_url = window.location.origin;
var district_id = $('#district_id').val().trim();

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/district/"+ district_id +"/maleAndFemalePopulation",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfPopulationMaleVSFemale");
            ctx.height = 200;

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata,
                height: ctx.height
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/district/"+ district_id +"/districtAndProvincePopulation",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfPopulationDistrictVSProvince");
            ctx.height = 200;

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata,
                height: ctx.height
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/district/"+district_id+"/districtAndProvinceArea",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var area = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                area.push(data[i].area);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: area
                    }
                ]
            };

            var ctx = $("#graphOfAreaDistrictVSProvince");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/district/"+ district_id+ "/population",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        // borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        // hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfBodyVsPopulation");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/district/"+district_id+"/area",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var area = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                area.push(data[i].area);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: area
                    }
                ]
            };

            var ctx = $("#graphOfBodyVsArea");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});


function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};