var base_url = window.location.origin;
var province_id = $('#province-id').val().trim();

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/province/"+ province_id +"/maleAndFemalePopulation",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfPopulationMaleVSFemale");
            ctx.height = 200;

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata,
                height: ctx.height
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/province/"+ province_id +"/provinceAndNepalPopulation",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfPopulationProvinceVSNepal");
            ctx.height = 200;

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata,
                height: ctx.height
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/province/"+province_id+"/provinceAndNepalArea",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var area = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                area.push(data[i].area);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: area
                    }
                ]
            };

            var ctx = $("#graphOfAreaProvinceVSNepal");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/province/"+ province_id+ "/population",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        // borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        // hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfDistrictVsPopulation");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/province/"+province_id+"/area",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var area = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                area.push(data[i].area);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: area
                    }
                ]
            };

            var ctx = $("#graphOfDistrictVsArea");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});


function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};