var base_url = window.location.origin;
var body_id = $('#body_id').val().trim();

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/body/"+ body_id +"/maleAndFemalePopulation",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfPopulationMaleVSFemale");
            ctx.height = 200;

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata,
                height: ctx.height
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/body/"+ body_id +"/bodyAndDistrictPopulation",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfPopulationBodyVSDistrict");
            ctx.height = 200;

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata,
                height: ctx.height
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/body/"+body_id+"/bodyAndDistrictArea",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var area = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                area.push(data[i].area);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: area
                    }
                ]
            };

            var ctx = $("#graphOfAreaBodyVSDistrict");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url +"/api/body/"+ body_id+ "/population",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var population = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                population.push(data[i].population);
                coloR.push(dynamicColors());
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Population',
                        backgroundColor: coloR,
                        // borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        // hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: population
                    }
                ]
            };

            var ctx = $("#graphOfWardsVsPopulation");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/body/"+body_id+"/area",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var area = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                area.push(data[i].area);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: area
                    }
                ]
            };

            var ctx = $("#graphOfWardsVsArea");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/body/"+body_id+"/otherInformation",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var number = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                number.push(data[i].number);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: number
                    }
                ]
            };

            var ctx = $("#bar1");

            var barGraph = new Chart(ctx, {
                type: 'bar',
                data: chartdata,
                options: {
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    $.ajax({
        url: base_url + "/api/body/"+body_id+"/otherInformation2",
        method: "GET",
        success: function(data) {
            console.log(data);
            var name = [];
            var number = [];
            var coloR = [];

            for(var i in data) {
                name.push( data[i].name);
                number.push(data[i].number);
                coloR.push(dynamicColors());
            }

            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Area',
                        backgroundColor: coloR,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: number
                    }
                ]
            };

            var ctx = $("#bar2");

            var barGraph = new Chart(ctx, {
                type: 'bar',
                data: chartdata,
                options: {
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

// var ctx = document.getElementById("schoolOfTheBody");
// ctx.height = 200;
// var barGraphSchool = new Chart(ctx, {
//     type: 'bar',
//     data: {
//         labels: ["Primary School", "Secondary School", "Higher Secondary School", "Universities", "hospitals", "banks", "roads", "solar", "bio-gas"],
//         datasets: [
//             {
//                 label: "Some extra data's about the body",
//                 backgroundColor: [
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors()
//
//                 ],
//                 borderColor: [
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                     dynamicColors(),
//                 ],
//                 borderWidth: 1,
//                 data: [20, 10, 30, 1, 23, 32, 12, 21, 50],
//             }
//         ]
//     },
//     options: {
//         scales: {
//             xAxes: [{
//                 stacked: true
//             }],
//             yAxes: [{
//                 stacked: true
//             }]
//         }
//     }
// });

function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};