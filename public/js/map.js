var myOption = {
    zoom: 7,
    center: new google.maps.LatLng(28.216018, 83.989341),
    scrollwheel: false,
    disableDoubleClickZoom: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    hide: [
        {
            featureType: 'poi.business',
            stylers: [{visibility: 'off'}]
        },
        {
            featureType: 'transit',
            elementType: 'labels.icon',
            stylers: [{visibility: 'off'}]
        }
    ],
    styles: [
        {
            "featureType": "all",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "weight": "2.00"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#9c9c9c"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#7b7b7b"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#46bcec"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#c8d7d4"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#070707"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        }
    ]
};

var map = new google.maps.Map(document.getElementById("map"),myOption);

provinces = new google.maps.Data();
// districts = new google.maps.Data();
// bodies = new google.maps.Data();

// var province = $('#district-name').val().trim();

var base_url = window.location.origin;
var locale = $('#locale').val().trim();

function loadMap(){

    // var bounds = new google.maps.LatLngBounds();
    // map.data.addListener('addfeature', function(e) {
    //   processPoints(e.feature.getGeometry(), bounds.extend, bounds);
    //   map.fitBounds(bounds);
    // });

    // clear_map_data();

    addProvinces();
}

function addProvinces(){

    // clear_map_data();

    // districts.setMap(null);
    // bodies.setMap(null);

    provinces.loadGeoJson('js/geoJSON/provinces.json');
    // provinces.loadGeoJson('geoJSON/provinces.json');

    provinces.setMap(map);

    var bounds = new google.maps.LatLngBounds();
    provinces.addListener('addfeature', function(e){
        processPoints(e.feature.getGeometry(), bounds.extend, bounds);
        map.fitBounds(bounds);
        map.setZoom(7);
    });

    //Styling the Provinces
    provinces.setStyle(function(feature){
        color = feature.getProperty('color');

        return /** @type {google.maps.Data.StyleOptions} */({
            fillColor: color,
            strokeColor: 'white',
            strokeWeight: 1,
            fillOpacity: 1
        });
    });

    provinces.addListener('mouseover', function(event) {
        provinces.revertStyle();
        provinces.overrideStyle(event.feature, {
            fillColor: '#69F0AE',
            strokeColor: 'white'
        });

        $("#hover").html(
            "<span>"+event.feature.getProperty("name")
            +"</span><br> <p>"+
            "Metropolitian: "+ event.feature.getProperty("metro") + "<br>" +
            "Sub-Metropolitian: "+ event.feature.getProperty("sub_metro") + "<br>" +
            "Municipalities: "+ event.feature.getProperty("muni") + "<br>" +
            "Rural-Municipalities: "+ event.feature.getProperty("rural") + "<br>" +
            "Wards: "+ event.feature.getProperty("ward") + "<br>" +
            "Population: "  + "<br>"
            +"</p>"
        );

        $("#hover").css({ left: event.pageX , top: event.pageY });
        $("#hover").show();

    });

    provinces.addListener('mouseout', function(event) {
        provinces.revertStyle();
        $("#hover").hide();
    });


    provinces.addListener('click', function(event) {

        window.open(base_url + "/"+locale+"/province/" + event.feature.getProperty("label"),"_self");

        // provinces.revertStyle();
        //
        // var bounds = new google.maps.LatLngBounds();
        // processPoints(event.feature.getGeometry(), bounds.extend, bounds);
        // map.fitBounds(bounds);
        //
        // addDistrict(event.feature.getProperty('label'));
        //
        // document.getElementById("name").innerHTML = event.feature.getProperty("name_en");
        //
        // "Metropolitian: "+ event.feature.getProperty("metro") + "<br>" +
        // "Sub-Metropolitian: "+ event.feature.getProperty("sub_metro") + "<br>" +
        // "Municipalities: "+ event.feature.getProperty("muni") + "<br>" +
        // "Rural-Municipalities: "+ event.feature.getProperty("rural") + "<br>" +
        // "Wards: "+ event.feature.getProperty("ward") + "<br>" +
        // "Population: "  + "<br>"
        //
        //
        // document.getElementById("mahanagar").innerHTML = event.feature.getProperty("metro");
        // document.getElementById("upnagar").innerHTML = event.feature.getProperty("sub_metro");
        // document.getElementById("nagar").innerHTML = event.feature.getProperty("muni");
        // document.getElementById("gau").innerHTML = event.feature.getProperty("rural");
        // document.getElementById("wards").innerHTML = event.feature.getProperty("ward");
        //
        //add breadCrumb for district
        

    });

}

// function addDistrict(province){
//
//     clear_map_data();
//
//     provinces.setMap(null);
//     districts.setMap(null);
//     bodies.setMap(null);
//
//     districts.loadGeoJson('js/geoJSON/provinces/province' + province + '.json');
//
//     districts.setMap(map);
//
//     var bounds = new google.maps.LatLngBounds();
//     districts.addListener('addfeature', function(e){
//         processPoints(e.feature.getGeometry(), bounds.extend, bounds);
//         map.fitBounds(bounds);
//         map.setZoom(8);
//     });
//
//     districts.setStyle(function(feature){
//         color = feature.getProperty('color');
//
//         return /** @type {google.maps.Data.StyleOptions} */({
//             fillColor: color,
//             strokeColor: 'white',
//             strokeWeight: 2,
//             fillOpacity: 1
//         });
//     });
//
//     districts.addListener('mouseover', function(event) {
//         districts.revertStyle();
//         districts.overrideStyle(event.feature, {
//             fillColor: '#69F0AE',
//             strokeColor: 'white'
//         });
//
//         //$("#hover").html("<span>"+event.feature.getProperty("district")+"</span><br> <p>"+(event.feature.getProperty("Summary")).replace(/;/g," </p><br>"));
//         $("#hover").html("<span>" + "Districe:  " + event.feature.getProperty("TARGET")+"</span><br> <p>"+ "Population: <br> Area: <br> Metropolitian: <br> Sub-MetroPolitian: <br> Municipality: <br> Rural-Municipality " + "</p>");
//         $("#hover").css({ left: event.pageX , top: event.pageY });
//         $("#hover").show();
//
//     });
//
//     districts.addListener('mouseout', function(event) {
//         districts.revertStyle();
//         $("#hover").hide();
//     });
//
//     districts.addListener('click', function(event) {
//
//         provinces.revertStyle();
//
//         var bounds = new google.maps.LatLngBounds();
//         processPoints(event.feature.getGeometry(), bounds.extend, bounds);
//         map.fitBounds(bounds);
//
//         // event.feature.getProperty('label')
//         addBodies(event.feature.getProperty('TARGET'));
//
//     });
// }

// function addBodies(district){
//
//     clear_map_data();
//
//     provinces.setMap(null);
//     districts.setMap(null);
//
//     bodies.loadGeoJson('js/geoJSON/districts/'+district.toLowerCase()+'.json');
//
//     bodies.setMap(map);
//
//     var bounds = new google.maps.LatLngBounds();
//     bodies.addListener('addfeature', function(e){
//         processPoints(e.feature.getGeometry(), bounds.extend, bounds);
//         map.fitBounds(bounds);
//         map.setZoom(10);
//     });
//
//     bodies.setStyle(function(feature){
//         color = '#AB47BC';
//
//         return /** @type {google.maps.Data.StyleOptions} */({
//             fillColor: color,
//             strokeColor: 'white',
//             strokeWeight: 2,
//             fillOpacity: 1
//         });
//     });
//
//     bodies.addListener('mouseover', function(event) {
//         districts.revertStyle();
//         districts.overrideStyle(event.feature, {
//             fillColor: '#69F0AE',
//             strokeColor: 'white'
//         });
//
//         //$("#hover").html("<span>"+event.feature.getProperty("district")+"</span><br> <p>"+(event.feature.getProperty("Summary")).replace(/;/g," </p><br>"));
//         $("#hover").html("<span>" + "" + event.feature.getProperty("GaPa_NaPa"));
//         $("#hover").css({ left: event.pageX , top: event.pageY });
//         $("#hover").show();
//
//     });
//
//     bodies.addListener('mouseout', function(event) {
//         districts.revertStyle();
//         $("#hover").hide();
//     });
//     //
//     // districts.addListener('click', function(event) {
//     //
//     //     provinces.revertStyle();
//     //
//     //     var bounds = new google.maps.LatLngBounds();
//     //     processPoints(event.feature.getGeometry(), bounds.extend, bounds);
//     //     map.fitBounds(bounds);
//     //
//     //     //addDistrict(event.feature.getProperty('Province'));
//     //
//     // });
// }
//


// function clear_map_data() {
//     map.data.forEach(function (feature) {
//         map.data.remove(feature);
//     });
// }


// Adds a marker to the map.
// function addMarker(location, map, label) {
//     // Add the marker at the clicked location, and add the next-available label
//     // from the array of alphabetical characters.
//     var marker = new google.maps.Marker({
//         position: location,
//         label: label,
//         map: map
//     });
// }

function processPoints(geometry, callback, thisArg) {
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    } else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    } else {
        geometry.getArray().forEach(function(g) {
            processPoints(g, callback, thisArg);
        });
    }
}

google.maps.event.addDomListener(window, 'load', loadMap);


//function to make mover follow the mouse
window.onmousemove=function(e){
    crdx = e.clientX;
    crdy = e.clientY;
    $("#hover").css('top',crdy+10).css('left',crdx+15);
};