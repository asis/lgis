var ctx = document.getElementById("myChart");
ctx.height = 200;

var myPieChart = new Chart(ctx,{
    type: 'pie',
    data :{
        labels: [
            "Male",
            "Female",
        ],
        datasets: [
            {
                data: [3100, 3500],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

var ctx = document.getElementById("myChart2");
ctx.height = 200;
var myPieChart = new Chart(ctx,{
    type: 'pie',
    data :{
        labels: [
            "Population of Province 4",
            "Remaining population of Nepal",
        ],
        datasets: [
            {
                data: [310000, 850000],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

var ctx = document.getElementById("myChart3");
ctx.height = 200;
var myPieChart = new Chart(ctx,{
    type: 'doughnut',
    data :{
        labels: [
            "Area of Province 4",
            "Remaining Area of Nepal",
        ],
        datasets: [
            {
                data: [12345, 1234567],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

var ctx = document.getElementById("myChart4");
ctx.height = 200;
var myPieChart = new Chart(ctx,{
    type: 'pie',
    data :{
        labels: [
            "Some information",
            "other information",
            "other information",
            "more information",
        ],
        datasets: [
            {
                data: [12345, 12345,31231, 31213],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FF3684",
                    "#36AC5B",
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FF3684",
                    "#36AC5B",
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

var ctx = document.getElementById("myChart5");
ctx.height = 200;
var myPieChart = new Chart(ctx,{
    type: 'doughnut',
    data :{
        labels: [
            "Some information",
            "other information",
            "other information",
            "more information",
        ],
        datasets: [
            {
                data: [12345, 12345,1231, 21331],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FF3684",
                    "#36AC5B",
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FF3684",
                    "#36AC5B",
                ]
            }]
    },
    options: {
        animation:{
            animateScale:true
        }
    }
});

var ctx = document.getElementById("myChart6");
ctx.height = 100;
var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "My First dataset",
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1,
                data: [65, 59, 80, 81, 56, 55, 40],
            }
        ]
    },
    options: {
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        }
    }
});/**
 * Created by asis on 5/7/17.
 */
