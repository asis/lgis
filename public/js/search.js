console.log("Function loaded");

var base_url = window.location.origin;
var locale = $('#locale').val().trim();

$(document).ready(function(){
    $('#searchbox').on('keyup',function () {

        $value = $(this).val();
        // alert($value);
        $.ajax({
            type : 'GET',
            url : '/api/search',
            data : {'search' : $value},
            success: function (data) {

                var options = '';

                for(var i = 0; i < data[0].length; i++)
                    options += "<option value=" + data[0][i] +" id =" + base_url + '/' + locale + '/body/'+ data[1][i] + " />";

                document.getElementById('livesearch').innerHTML = options;


            }
        })
    });
    $('#searchbutton').click(function () {
        var textval = $('#searchbox').val();
        $('#livesearch option').each(function () {
            if ($(this).val() == textval){
                // alert("clicked");
                window.location = $(this).attr('id');
            }
        })
    })



});





// $(document).ready(function(){
//     console.log("working");
//     $('#searchbox').selectize({
//
//         valueField: 'url',
//         labelField: 'name',
//         searchField: ['name'],
//         maxOptions: 10,
//         options: [],
//         create: false,
//         render: {
//             option: function(item, escape) {
//                 return '<div>' +escape(item.name)+'</div>';
//             }
//         },
//         optgroups: [
//             {value: 'body', label: 'Local Units'},
//             // {value: 'category', label: 'Categories'}
//         ],
//         optgroupField: 'class',
//         optgroupOrder: ['body'],
//         load: function(query, callback) {
//             if (!query.length) return callback();
//             $.ajax({
//                 url: root+'/api/search',
//                 type: 'GET',
//                 dataType: 'json',
//                 data: {
//                     q: query
//                 },
//                 error: function() {
//                     callback();
//                 },
//                 success: function(res) {
//                     callback(res.data);
//                 }
//             });
//         },
//         onChange: function(){
//             window.location = this.items[0];
//         }
//     });
//
// });