var myOption = {
    zoom: 7,
    center: new google.maps.LatLng(28.216018, 83.989341),
    scrollwheel: false,
    disableDoubleClickZoom: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    hide: [
        {
            featureType: 'poi.business',
            stylers: [{visibility: 'off'}]
        },
        {
            featureType: 'transit',
            elementType: 'labels.icon',
            stylers: [{visibility: 'off'}]
        }
    ],
    styles: [
        {
            "featureType": "all",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "weight": "2.00"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#9c9c9c"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#7b7b7b"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#46bcec"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#c8d7d4"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#070707"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        }
    ]
};

var map = new google.maps.Map(document.getElementById("map"),myOption);

var districtname = $('#district-name').val().trim();

provinces = new google.maps.Data();

var base_url = window.location.origin;
var locale = $('#locale').val().trim();

function loadMap(){

    addProvinces();
}

function addProvinces(){

    provinces.loadGeoJson(base_url + '/js/geoJSON/districts/'+ districtname.toLowerCase() + '.json');

    provinces.setMap(map);

    var bounds = new google.maps.LatLngBounds();
    provinces.addListener('addfeature', function(e){
        processPoints(e.feature.getGeometry(), bounds.extend, bounds);
        map.fitBounds(bounds);
        map.setZoom(10);
    });

    //Styling the Provinces
    provinces.setStyle(function(feature){
        color = feature.getProperty('color');

        return /** @type {google.maps.Data.StyleOptions} */({
            fillColor: '#6391f0',
            strokeColor: 'white',
            strokeWeight: 1,
            fillOpacity: 0.3
        });
    });

    provinces.addListener('mouseover', function(event) {
        provinces.revertStyle();
        provinces.overrideStyle(event.feature, {
            fillColor: '#69F0AE',
            strokeColor: 'white'
        });

        $("#hover").html(
            "<span>"+event.feature.getProperty("GaPa_NaPa")

        );

        $("#hover").css({ left: event.pageX , top: event.pageY });
        $("#hover").show();

    });

    provinces.addListener('mouseout', function(event) {
        provinces.revertStyle();
        $("#hover").hide();
    });

    provinces.addListener('click', function (event) {
        var body = event.feature.getProperty("GaPa_NaPa").toLowerCase();
        console.log(body);
        document.getElementById(body).scrollIntoView();
    })
}

function processPoints(geometry, callback, thisArg) {
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    } else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    } else {
        geometry.getArray().forEach(function(g) {
            processPoints(g, callback, thisArg);
        });
    }
}

google.maps.event.addDomListener(window, 'load', loadMap);


//function to make mover follow the mouse
window.onmousemove=function(e){
    crdx = e.clientX;
    crdy = e.clientY;
    $("#hover").css('top',crdy+10).css('left',crdx+15);
};