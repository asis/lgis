<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/',function (){
    return response()->json([
        'get List of Provinces' => '{locale}/province/',
        'get Detail of a Province' => '{locale}/province/{id}',

        'get List of Districts' => '{locale}/district/',
        'get Detail of a District' => '{locale}/district/{id}',

        'get List of Local Bodies'=> '{locale}/body/{locale}',
        'get Detail of a Local Body' => '{locale}/body/{id}',

        'GeoJSON of all Provinces of Nepal'=> '{locale}/geoJSON/province'
    ]);
});

Route::get('{locale}/geoJSON/province', 'ProvinceController@createGeoJSON');

Route::get('{locale}/province/','ProvinceController@index');

Route::get('{locale}/province/{id}','ProvinceController@show');

Route::get('{locale}/district/', 'DistrictController@index');

Route::get('{locale}/district/{id}', 'DistrictController@show');

Route::get('{locale}/body/', 'BodyController@index');

Route::get('{locale}/body/{id}', 'BodyController@show');


//get the data for chart
Route::get('/body/{id}/area', 'BodyController@areaOfWards');
Route::get('/body/{id}/population', 'BodyController@populationOfWards');
Route::get('/body/{id}/maleAndFemalePopulation', 'BodyController@populationOfMaleAndFemale');
Route::get('/body/{id}/bodyAndDistrictPopulation', 'BodyController@populationOfBodyAndDistrict');
Route::get('/body/{id}/bodyAndDistrictArea', 'BodyController@areaOfBodyAndDistrict');
Route::get('/body/{id}/otherInformation', 'BodyController@otherBodyInformation');
Route::get('/body/{id}/otherInformation2', 'BodyController@otherBodyInformation2');

Route::get('/district/{id}/area', 'DistrictController@areaOfBodies');
Route::get('/district/{id}/population', 'DistrictController@populationOfBodies');
Route::get('/district/{id}/maleAndFemalePopulation', 'DistrictController@populationOfMaleAndFemale');
Route::get('/district/{id}/districtAndProvincePopulation', 'DistrictController@populationOfDistrictAndProvince');
Route::get('/district/{id}/districtAndProvinceArea', 'DistrictController@areaOfDistrictAndProvince');

Route::get('/province/{id}/area', 'ProvinceController@areaOfDistricts');
Route::get('/province/{id}/population', 'ProvinceController@populationOfDistricts');
Route::get('/province/{id}/maleAndFemalePopulation', 'ProvinceController@populationOfMaleAndFemale');
Route::get('/province/{id}/provinceAndNepalPopulation', 'ProvinceController@populationOfProvinceAndNepal');
Route::get('/province/{id}/provinceAndNepalArea', 'ProvinceController@areaOfProvinceAndNepal');


Route::get('/search', 'NepalController@livesearch');
