<?php
//Route::get('/id_manager','NepalController@id_manager');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * For Admin Panel
 *
 * */

//Auth::routes();
//Route::get('/welcome', function (){
//    return view('welcome');
//});

//Route::get('lgisadminpanel/login', 'AdminController@login');
Route::get( 'lgisadminpanel/login', ['middleware' => 'guest', 'uses' => 'AdminController@login'] );

Route::post('lgisadminpanel/login', 'AdminController@postLogin');
//Route::post('lgisadminpanel/login/', ['as' => 'lgisadminpanel.login', 'uses' => 'AdminController@postLogin']);

Route::group(['middleware' => 'auth'], function()
{
    Route::get('lgisadminpanel',          				['uses' => 'AdminController@index']				    );
    Route::get('lgisadminpanel/suggestion/{id}','AdminController@detail');
    Route::get('lgisadminpanel/suggestion/delete/{id}','AdminController@delete');

    Route::resource('lgisadminpanel/province', 'AdminProvinceController');
    Route::get('lgisadminpanel/province/get_from_web/{id}','AdminProvinceController@get_data_from_web');

    Route::resource('lgisadminpanel/district', 'AdminDistrictController');
    Route::get('lgisadminpanel/district/get_from_web/{id}','AdminDistrictController@get_data_from_web');

    Route::resource('lgisadminpanel/body', 'AdminBodyController');
    Route::get('lgisadminpanel/body/get_from_web/{id}','AdminBodyController@get_data_from_web');
    Route::get('lgisadminpanel/body/otherinformation/{id}','AdminBodyController@getOtherInformationForm');
    Route::post('lgisadminpanel/body/otherinformation/{id}','AdminBodyController@postOtherInformationForm');


    Route::get('lgisadminpanel/ward/create/{id}', 'AdminWardController@create');
    Route::resource('lgisadminpanel/ward', 'AdminWardController');

});

Route::get('lgisadminpanel/logout', 'AdminController@getLogout');

/*
 * For Web Application
 *
 * */

Route::get('/',function (){
    $locale = Session::get ('locale');
    if(empty($locale)){
        Session::put('locale','en');
        $locale = 'en';
    }
    return redirect('/' . $locale);
});

Route::get('{locale}/contact', function ($locale){
    \App::setLocale($locale);
    return view('app.site.contact');
});

Route::get('{locale}/about', function ($locale){
    \App::setLocale($locale);
    return view('app.site.about');
});

Route::get('{locale}/help', function ($locale){
    \App::setLocale($locale);
    return view('app.site.help');
});

Route::get('/{locale}', 'NepalController@index');
Route::get('/{locale}/body/metro', 'NepalController@getMetro');
Route::get('/{locale}/body/submetro', 'NepalController@getSubMetro');
Route::get('/{locale}/body/munic', 'NepalController@getMunic');
Route::get('/{locale}/body/ruralmunic', 'NepalController@getRuralMuni');

//Route::get('/{locale}/search', 'NepalController@livesearch');

Route::post('/nepal/', ['as' => 'suggestion_store', 'uses' => 'NepalController@store']);

Route::get('{locale}/province','ProvinceController@index');
Route::get('/{locale}/province/{id}','ProvinceController@show');


Route::get('/{locale}/district', 'DistrictController@index');
Route::get('/{locale}/district/{id}', 'DistrictController@show');


Route::get('/{locale}/body', 'BodyController@index');
Route::get('/{locale}/body/{id}', 'BodyController@show');






