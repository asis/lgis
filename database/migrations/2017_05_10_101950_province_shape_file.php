<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProvinceShapeFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces_shape_file', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('province_id');
            $table->longText('lat_lng');
            $table->string('color');
            $table->timestamps();
        });

        Schema::table('provinces_shape_file', function(Blueprint $table){
            $table->foreign('province_id')->references('id')->on('provinces')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces_shape_file');
    }
}
