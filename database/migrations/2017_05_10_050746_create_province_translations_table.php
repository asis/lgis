<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvinceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('province_id')->unsigned();
            $table->char('language',2);
            $table->boolean('is_default');
            $table->string('name')->collation('utf8mb4_unicode_ci');
            $table->text('tag')->collation('utf8mb4_unicode_ci');
            $table->text('description')->collation('utf8mb4_unicode_ci');
            $table->string('headquarter')->collation('utf8mb4_unicode_ci');
            $table->string('population')->collation('utf8mb4_unicode_ci');
            $table->string('area')->collation('utf8mb4_unicode_ci');
            $table->string('density')->collation('utf8mb4_unicode_ci');
            $table->string('border')->collation('utf8mb4_unicode_ci');
            $table->timestamps();
        });

        Schema::table('province_translations', function(Blueprint $table){
            $table->foreign('province_id')->references('id')->on('provinces')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province_translations');
    }
}
