<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistrictTranslation extends Model
{
    protected $fillable = ['language', 'district_id','is_default','name','description', 'headquarter', 'population','male_population','female_population', 'area', 'density', 'border'];

    public function post()
    {
        return $this->belongsTo('App\District');
    }
}
