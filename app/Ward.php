<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $fillable = [
        'name', 'population', 'area', 'local_body_id'
    ];

    public function body(){
        return $this->belongsTo('App\Body');
    }
}
