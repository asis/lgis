<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvincesShapeFile extends Model
{
    protected $table = 'provinces_shape_file';

    protected $fillable = ['lat_lng','color'];

    public function province(){
        return $this->belongsTo('App\Province');
    }
}
