<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representative extends Model
{
    protected $fillable = ['id'];

    public function body(){
        return $this->belongsTo('App\body','body_id');

    }

}
