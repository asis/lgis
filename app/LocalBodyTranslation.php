<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalBodyTranslation extends Model
{
    protected $fillable = ['language', 'local_body_id','is_default','name', 'description','headquarter','male_population','mayor','deputy','female_population' , 'population', 'area', 'density', 'border','ward_mix_of'];

    public function post()
    {
        return $this->belongsTo('App\LocalBody')->orderBy('name');
    }
}
