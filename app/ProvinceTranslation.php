<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvinceTranslation extends Model
{

    //'language', 'province_id','is_default',
    protected $fillable = ['name','description', 'headquarter','male_population','female_population', 'population', 'area', 'density', 'border'];

    public function post()
    {
        return $this->belongsTo('App\Province');
    }
}
