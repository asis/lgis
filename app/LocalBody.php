<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use App\Support\Translateable;

class LocalBody extends Model
{
    //This class will force the Translated class into main class
    use Translateable;

    protected $fillable = ['province_id','district_id', 'type', 'ward_no'];

    public function translation($language = null)
    {
        if ($language == null) {
            $language = App::getLocale();
        }
        return $this->hasMany('App\LocalBodyTranslation')->where('language', '=', $language);
    }

    public function province(){
        return $this->belongsTo('App\Province');
    }

    public function district(){
        return $this->belongsTo('App\District');
    }

    public function ward(){
        return $this->hasMany('App\Ward');
    }
    public function representative(){
        return $this->hasOne('App\Representative');
    }
}
