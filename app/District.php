<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use App\Support\Translateable;

class District extends Model
{
    //This class will force the Translated class into main class
    use Translateable;

    protected $fillable = ['province_id'];

    public function translation($language = null)
    {
        if ($language == null) {
            $language = App::getLocale();
        }
        return $this->hasMany('App\DistrictTranslation')->where('language', '=', $language);
    }

    public function bodies(){
        return $this->hasMany('App\LocalBody','district_id');
    }

    public function province(){
        return $this->belongsTo('App\Province');
    }
}
