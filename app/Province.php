<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use App\Support\Translateable;

class Province extends Model
{

    protected $fillable = ['image_name'];

    //This class will force the Translated class into main class
    use Translateable;

    public function translation($language = null)
    {
        if ($language == null) {
            $language = App::getLocale();
        }
        return $this->hasMany('App\ProvinceTranslation')->where('language', '=', $language);
    }

    public function districts(){
        return $this->hasMany('App\District','province_id');
    }

    public function provinceShapeFile(){
        return $this->hasOne('App\ProvincesShapeFile','province_id');
    }

    public function body(){
        return $this->hasMany('App\LocalBody','province_id');
    }

    public function bodies()
    {
        return $this->hasManyThrough('App\LocalBody', 'App\District','province_id', 'district_id', 'id');
    }

}
