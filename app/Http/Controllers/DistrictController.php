<?php

namespace App\Http\Controllers;

use App\DistrictTranslation;
use App\Suggestion;
use Illuminate\Http\Request;

use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use App\District;

class DistrictController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param $locale
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {

        \App::setLocale($locale);

        $districts = District::all();

        $districts = $districts->sort(function($a, $b)
        {
            $a = $a->translation()->first()->name;
            $b = $b->translation()->first()->name;
            //here you can do more complex comparisons
            //when dealing with sub-objects and child models
            if ($a === $b) {
                return 0;
            }
            return ($a > $b) ? 1 : -1;
        });

        $districts = $this::paginate($districts, 5);


        if ( \Request::is('api/*')) {

            //for api fetch all district

            $districts = District::all();

            $district = array();
            foreach ($districts as $d) {
                array_push($district, array(
                        'province_id' => $d->province_id,
                        'district_id' => $d->id,
                        'district_name' => $d->translation()->first()->name,
                        'district_description' => $d->translation()->first()->description,
                        'count_bodies' => count($d->bodies),
                        'count_wards' => $d->bodies->sum('ward_no'),
                    )
                );
            }
            return response()->json($district);
        }

        return view('app.district.index')->with('districts', $districts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param $locale
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale,$id)
    {

        \App::setLocale($locale);

        $district = District::findOrFail($id);
        $bodies = $district->bodies;

        if (\Request::is('api/*')) {

            $district_details = array();
            array_push($district_details, array(
                    'district_id' => $district->id,
                    'province_id' => $district->province_id,
                    'district_name' => $district->translation()->first()->name,
                    'district_description' => $district->translation()->first()->description,
                    'ward_count' => $bodies->sum('ward_no'),
                    'bodies' => $bodies,
                )
            );

            return response()->json($district_details);
        }

        return view('app.district.details')->with('district', $district);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function paginate($items, $perPage)
    {
        $pageStart = request('page', 1);
        $offSet    = ($pageStart * $perPage) - $perPage;
        $itemsForCurrentPage = $items->slice($offSet, $perPage);

        return new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsForCurrentPage, $items->count(), $perPage,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }

    public function areaOfBodies($id){

        if ( \Request::is('api/*')) {

            $district = District::findOrFail($id);
            $bodies = $district->bodies;

            $data = array();

            foreach ($bodies as $body) {
                array_push($data, array(
                    'name' => $body->translation('en')->first()->name,
                    'area' => (double) $body->translation('en')->first()->area
                ));
            }

            return response()->json($data);
        }
    }

    public function populationOfBodies($id){
        if ( \Request::is('api/*')) {
            $district = District::findOrFail($id);
            $bodies = $district->bodies;

            $data = array();

            foreach ($bodies as $body) {
                array_push($data, array(
                    'name' => $body->translation('en')->first()->name,
                    'population' => (double) $body->translation('en')->first()->population
                ));
            }
            return response()->json($data);
        }
    }

    public function populationOfMaleAndFemale($id){
        if ( \Request::is('api/*')) {
            $district = District::findOrFail($id);

            $data = array();
            array_push($data,[
                'name' => 'Male Population',
                'population' => (double) $district->translation('en')->first()->male_population
            ]);

            array_push($data,[
                'name' => 'Female Population',
                'population' => (double) $district->translation('en')->first()->female_population
            ]);

            return response()->json($data);
        }
    }

    public function populationOfDistrictAndProvince($id){
        if ( \Request::is('api/*')) {
            $district = District::findOrFail($id);

            $data = array();
            array_push($data,[
                'name' => 'Population of Province',
                'population' => (double) $district->province->translation('en')->first()->population
            ]);

            array_push($data,[
                'name' => 'Population of District',
                'population' => (double) $district->translation('en')->first()->population
            ]);

            return response()->json($data);
        }
    }

    public function areaOfDistrictAndProvince($id){
        if ( \Request::is('api/*')) {
            $district = District::findOrFail($id);

            $data = array();
            array_push($data,[
                'name' => 'Area of Province',
                'area' => (double) $district->province->translation('en')->first()->area
            ]);

            array_push($data,[
                'name' => 'Area of District',
                'area' => (double) $district->translation('en')->first()->area
            ]);

            return response()->json($data);
        }
    }
}