<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\District;

use Intervention\Image\Facades\Image;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;


class AdminDistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = District::paginate(12);
        return view('admin.pages.district.index')->with('districts', $districts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('admin.pages.province.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Locale $locale
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $district = District::findOrFail($id);

        return view('admin.pages.district.show')->with('district', $district);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $district = District::findOrFail($id);

        return view('admin.pages.district.edit')->with('district', $district);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//dd($id);
        $district = District::findOrFail($id);

//        $this->validate($request, [
//            'name' => 'required',
//            'description' => 'required',
//            'headquarter' => 'required',
//            'population' => 'required',
//            'male_population' => 'required',
//            'female_population' => 'required',
//            'area' => 'required',
//            'density' => 'required',
////            'east_border' => 'required',
////            'west_border' => 'required',
////            'south_border' => 'required',
////            'north_border' => 'required',
//
//            'name_ne' => 'required',
//            'description_ne' => 'required',
//            'headquarter_ne' => 'required',
//            'area_ne' => 'required',
//            'density_ne' => 'required',
////            'east_border_ne' => 'required',
////            'west_border_ne' => 'required',
////            'south_border_ne' => 'required',
////            'north_border_ne' => 'required',
//        ]);

//        $request['border']  = 'east:'.$request['east_border'].';';
//        $request['border'] .= 'west:'.$request['west_border'].';';
//        $request['border'] .= 'south:'.$request['south_border'].';';
//        $request['border'] .= 'north:'.$request['north_border'];

        //dd($request['border']);

//        $request['border_ne']  = 'east:'.$request['east_border_ne'].';';
//        $request['border_ne'] .= 'west:'.$request['west_border_ne'].';';
//        $request['border_ne'] .= 'south:'.$request['south_border_ne'].';';
//        $request['border_ne'] .= 'north:'.$request['north_border_ne'];

        $input = $request->all();
//dd($input);

        //$province->fill($input)->save();

        $png_url = strtolower($request['name'].".png");
        $path = public_path().'/images/districts/' . $png_url;

        if($request->hasFile('map_upload')){

            Image::make(file_get_contents($request->map_upload))->save($path);
        }

        $district->translation('en')->first()->fill($input)->save();

        // for nepali storage
        $input['name'] = $request['name_ne'];
        $input['description'] = $request['description_ne'];
        $input['headquarter'] = $request['headquarter_ne'];
        $input['population'] = $request['population'];
        $input['male_population'] = $request['male_population'];
        $input['female_population'] = $request['female_population'];
        $input['area'] = $request['area'];
        $input['density'] = $request['density'];
//        $input['border'] = $request['border_ne'];

        $district->translation('ne')->first()->fill($input)->save();

        \Session::flash('message', 'Task successfully Edited!');
        return view('admin.pages.district.edit')->with(
            array(
                'district' => $district
            ));//        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_data_from_web($id)
    {
        $district = District::findOrFail($id);
        $districts = District::all();
        $ne_district_info = array();
        $nep = $district->translation('ne')->first()->name;
        $nep_explode = explode(' ', $nep);
        $last_word_nep = array_pop($nep_explode);
        $eng = $district->translation('en')->first()->name;
        $eng_explode = explode(' ', $eng);
        $last_word_eng = array_pop($eng_explode);

        $p_description_url_ne = "http://ne.wikipedia.org/wiki/$last_word_nep जिल्ला";
        $p_description_url_en = "http://en.wikipedia.org/wiki/$last_word_eng district";
//        dd($p_description_url_ne);
        $ne_client = new Client();
        //loop to get crawlers for indivisual urls
        $ne_crawlers = $ne_client->request('GET', $p_description_url_ne);
        $ne_district_info = $ne_crawlers->filter('p')->each(function (Crawler $node, $i) {
            return $ne_district_info[] = $node->text();
        });
        $ne_district_information = array_values(array_filter($ne_district_info, function ($value) {
            return $value !== '';
        }));
//        dd($ne_district_information);
        $en_client = new Client();
        //loop to get crawlers for indivisual urls
        $en_crawlers = $en_client->request('GET', $p_description_url_en);
        $en_district_info = $en_crawlers->filter('p')->each(function (Crawler $node, $i) {
            return $en_district_info[] = $node->text();
        });
        $en_district_information = array_values(array_filter($en_district_info, function ($value) {
            return $value !== '';
        }));

//        //for population
        $en_district_area = $en_crawlers->filter('.vcard > .mergedrow > td > a')->each(function (Crawler $node, $i) {
            return $en_district_pop[] = $node->text();
        });
//        dd($en_district_area);
        $key = "km2";
        $matches = array_filter($en_district_area, function($var) use ($key) { return preg_match("/\b$key\b/i", $var); });
        $area = array_merge($matches);

        if (empty($area)){
            $area[0] = "no data";
        };

        $en_district_den = $en_crawlers->filter('.vcard > .mergedrow > td')->each(function (Crawler $node, $i) {
            return $en_district_den[] = $node->text();
        });
//        dd($en_district_den);
        $key = "km2";
        $density_matches = array_filter($en_district_den, function($var) use ($key) { return preg_match("/\b$key\b/i", $var); });
        $density = array_merge($density_matches);

        if (empty($density)){
            array_push($density,"no data");
            array_push($density,"no data");
        };
        return view('admin.pages.district.edit')->with(
            array(
                'ne_district_info' => $ne_district_information,
                'en_district_info' => $en_district_information,
                'district' => $district,
                'area' => $area[0],
                'density' => $density[1]
            ));
    }


}
