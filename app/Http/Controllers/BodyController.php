<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LocalBody;
use Illuminate\Support\Facades\DB;


class BodyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param $locale
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {

        \App::setLocale($locale);

        $body = LocalBody::all();

        if ( \Request::is('api/*')) {

            //for api fetch all
            $bodies = LocalBody::all();

            $body = array();
            foreach ($bodies as $b) {
                array_push($body, array(
                        'district_id' => $b->district_id,
                        'province_id' => $b->province_id,
                        'body_name' => $b->translation()->first()->name,
                        'body_description' => $b->translation()->first()->description,
                        'type' => $b->type,
                        'ward' => $b->ward_no
                    )
                );
            }
            return response()->json($body);
        }

        $body = $body->sort(function($a, $b)
        {
            $a = $a->translation()->first()->name;
            $b = $b->translation()->first()->name;
            //here you can do more complex comparisons
            //when dealing with sub-objects and child models
            if ($a === $b) {
                return 0;
            }
            return ($a > $b) ? 1 : -1;
        });

        $body = $this::paginate($body, 5);

        return view('app.body.index')->with('bodies', $body);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale,$id)
    {
        \App::setLocale($locale);
        $body = LocalBody::findOrFail($id);

        if ( \Request::is('api/*')) {

            $body_detail = array();

            array_push($body_detail, array(
                    'district_id' => $body->district_id,
                    'province_id' => $body->province_id,
                    'body_name' => $body->translation()->first()->name,
                    'body_description' => $body->translation()->first()->description,
                    'type' => $body->type,
                    'ward' => $body->ward_no,
                )
            );
            return response()->json($body_detail);
        }

        return view('app.body.details')->with('body',$body);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function paginate($items, $perPage)
    {
        $pageStart = request('page', 1);
        $offSet    = ($pageStart * $perPage) - $perPage;
        $itemsForCurrentPage = $items->slice($offSet, $perPage);

        return new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsForCurrentPage, $items->count(), $perPage,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }

    public function areaOfWards($id){

        if ( \Request::is('api/*')) {

            $body = LocalBody::findOrFail($id);
            $wards = $body->ward;

            $data = array();

            foreach ($wards as $ward) {
                array_push($data, array(
                    'name' => $ward->name,
                    'area' => $ward->area
                ));
            }

            return response()->json($data);
        }
    }

    public function populationOfWards($id){
        if ( \Request::is('api/*')) {
            $body = LocalBody::findOrFail($id);
            $wards = $body->ward;

            $data = array();

            foreach ($wards as $ward) {
                array_push($data, array(
                    'name' => $ward->name,
                    'population' => $ward->population
                ));
            }
            return response()->json($data);
        }
    }

    public function populationOfMaleAndFemale($id){
        if ( \Request::is('api/*')) {
            $body = LocalBody::findOrFail($id);

            $data = array();
            array_push($data,[
                'name' => 'Male Population',
                'population' => (double) $body->translation('en')->first()->male_population
            ]);

            array_push($data,[
                'name' => 'Female Population',
                'population' => (double) $body->translation('en')->first()->female_population
            ]);

            return response()->json($data);
        }
    }

    public function populationOfBodyAndDistrict($id){
        if ( \Request::is('api/*')) {
            $body = LocalBody::findOrFail($id);

            $data = array();
            array_push($data,[
                'name' => 'Population of District',
                'population' => (double) $body->district->translation('en')->first()->population
            ]);

            array_push($data,[
                'name' => 'Population of Body',
                'population' => (double) $body->translation('en')->first()->population
            ]);

            return response()->json($data);
        }
    }

    public function areaOfBodyAndDistrict($id){
        if ( \Request::is('api/*')) {
            $body = LocalBody::findOrFail($id);

            $data = array();
            array_push($data,[
                'name' => 'Area of District',
                'area' => (double) $body->district->translation('en')->first()->area
            ]);

            array_push($data,[
                'name' => 'Area of Body',
                'area' => (double) $body->translation('en')->first()->area
            ]);

            return response()->json($data);
        }
    }

    public function otherBodyInformation($id){
        $information = DB::table('other_information')->where('local_body_id',$id)->first();

        $data = array();

        array_push($data,[
            'name' => 'Hospital',
            'number' => $information->hospital
        ]);

        array_push($data,[
            'name' => 'Health Centre',
            'number' => $information-> health_centre
        ]);

        array_push($data,[
            'name' => 'Health Post',
                'number' => $information->health_post
        ]);

        array_push($data,[
            'name' => 'Sub Health Post',
                'number' => $information->sub_health_post
        ]);


        array_push($data,[
            'name' => 'Ayurvedic Medicine Centre',
                'number' => $information->ayurvedic_medicine_centre,
        ]);

        array_push($data,[
            'name' => 'rural_health_social_volunteer',
                'number' => $information->rural_health_social_volunteer
        ]);

        array_push($data,[
            'name' => "child_care_centre",
                'number' => $information->child_care_centre
        ]);

        array_push($data,[
            'name' => 'primary_school',
                'number' => $information->primary_school
        ]);

        array_push($data,[
            'name' => 'secondary_school',
                'number' => $information->secondary_school
        ]);

        array_push($data,[
            'name' => 'College',
                'number' => $information->college
        ]);

        array_push($data,[
            'name' => "university",
                'number' => $information->university
        ]);

        //
        array_push($data,[
            'name' => 'banijya_bank',
                'number' => $information->banijya_bank
        ]);

        array_push($data,[
            'name' => 'development_bank',
                'number' => $information->development_bank
        ]);

        array_push($data,[
            'name' => 'financial_institution',
                'number' => $information->financial_institution
        ]);

        array_push($data,[
            'name' => 'laghu_bittiya_sanstha_ltd',
                'number' => $information->laghu_bittiya_sanstha_ltd
        ]);

        array_push($data,[
            'name' => 'saving_and_credit_cooperative_ltd',
                'number' => $information->saving_and_credit_cooperative_ltd
        ]);

        array_push($data,[
            'name' => 'pitch_road',
                'number' => $information->pitch_road
        ]);

        array_push($data,[
            'name' => 'semi_pitch_road',
                'number' => $information->semi_pitch_road
        ]);

        array_push($data,[
            'name' => 'rough_road',
                'number' => $information->rough_road
        ]);

        return response()->json($data);

    }

    function otherBodyInformation2($id){

        $information = DB::table('other_information')->where('local_body_id',$id)->first();

        $data = array();

        array_push($data,[
            'name' => 'driking_water_available',
                'number' => $information->driking_water_available
        ]);


        array_push($data,[
            'name' => 'public_toilet_available',
                'number' => $information->public_toilet_available
        ]);

        array_push($data,[
            'name' => 'center_for_media',
                'number' => $information->center_for_media
        ]);

        array_push($data,[
            'name' => 'micro_hydro',
                'number' => $information->micro_hydro
        ]);

        array_push($data,[
            'name' => 'Solar',
                'number' => $information->solar
        ]);


        array_push($data,[
            'name' => 'bio_gas',
                'number' => $information->bio_gas
        ]);

        array_push($data,[
            'name' => 'improved_stove',
                'number' => $information->improved_stove
        ]);

        return response()->json($data);
    }
}
