<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Province;

use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class AdminProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::all();

        return view('admin.pages.province.index')->with('provinces',$provinces);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.province.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Locale $locale
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $province = Province::findOrFail($id);

        return view('admin.pages.province.show')->with('province',$province);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $province = Province::findOrFail($id);

        return view('admin.pages.province.edit')->with('province',$province);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $province = Province::findOrFail($id);

        $this->validate($request, [
        'name' => 'required',
        'description' => 'required',
        'headquarter' => 'required',
        'population' => 'required',
        'area' => 'required',
        'density' => 'required',
//        'east_border' => 'required',
//        'west_border' => 'required',
//        'south_border' => 'required',
//        'north_border' => 'required',

        'name_ne' => 'required',
        'description_ne' => 'required',
        'headquarter_ne' => 'required',
        'population_ne' => 'required',
        'area_ne' => 'required',
        'density_ne' => 'required',
//        'east_border_ne' => 'required',
//        'west_border_ne' => 'required',
//        'south_border_ne' => 'required',
//        'north_border_ne' => 'required',
    ]);
//
//        $request['border']  = 'east:'.$request['east_border'].';';
//        $request['border'] .= 'west:'.$request['west_border'].';';
//        $request['border'] .= 'south:'.$request['south_border'].';';
//        $request['border'] .= 'north:'.$request['north_border'];

        //dd($request['border']);

//        $request['border_ne']  = 'east:'.$request['east_border_ne'].';';
//        $request['border_ne'] .= 'west:'.$request['west_border_ne'].';';
//        $request['border_ne'] .= 'south:'.$request['south_border_ne'].';';
//        $request['border_ne'] .= 'north:'.$request['north_border_ne'];

        $input = $request->all();

        //$province->fill($input)->save();

        $province->translation('en')->first()->fill($input)->save();

        // for nepali storage
        $input['name'] = $request['name_ne'];
        $input['description'] = $request['description_ne'];
        $input['headquarter'] = $request['headquarter_ne'];
        $input['population'] = $request['population_ne'];
        $input['area'] = $request['area_ne'];
        $input['density'] = $request['density_ne'];
//        $input['border'] = $request['border_ne'];

        $province->translation('ne')->first()->fill($input)->save();

        \Session::flash('message', 'Task successfully Edited!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function get_data_from_web($id)
    {

        $province = Province::findOrFail($id);
        $provinces = Province::all();
        $ne_province_info = array();
        $nep = $province->translation('ne')->first()->name;
        $nep_explode = explode(' ', $nep);
        $last_word_nep = array_pop($nep_explode);
        $eng = $province->translation('en')->first()->name;
        $eng_explode = explode(' ', $eng);
        $last_word_eng = array_pop($eng_explode);

        $p_description_url_ne = "http://ne.wikipedia.org/wiki/प्रदेश_नं._$last_word_nep";
        $p_description_url_en = "http://en.wikipedia.org/wiki/Province_No._$last_word_eng";
//        dd($p_description_url_ne);
        $ne_client = new Client();
        //loop to get crawlers for indivisual urls
        $ne_crawlers = $ne_client->request('GET', $p_description_url_ne);
        $ne_province_info = $ne_crawlers->filter('p')->each(function (Crawler $node, $i) {
            return $ne_province_info[] = $node->text();
        });
        $ne_province_information = array_values(array_filter($ne_province_info, function ($value) {
            return $value !== '';
        }));
        $en_client = new Client();
        //loop to get crawlers for indivisual urls
        $en_crawlers = $en_client->request('GET', $p_description_url_en);
        $en_province_info = $en_crawlers->filter('p')->each(function (Crawler $node, $i) {
            return $en_province_info[] = $node->text();
        });
        $en_province_information = array_values(array_filter($en_province_info, function ($value) {
            return $value !== '';
        }));

//        dd($en_province_information);
        return view('admin.pages.province.edit')->with(
            array(
                'ne_province_info' => $ne_province_information,
                'en_province_info' => $en_province_information,
                'province' => $province
            ));


    }
}
