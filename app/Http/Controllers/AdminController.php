<?php

namespace App\Http\Controllers;

use App\District;
use App\LocalBody;
use App\Province;

use App\Suggestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{

    public function index(){

        $suggestions = Suggestion::all()->sortByDesc('created_at');

        $suggestions = $this::paginate($suggestions, 6);

        foreach ($suggestions as $suggestion){
            $article_id = $suggestion->article_id;
            $article_type = $suggestion->type;
            $article_name = "";

            switch ($article_type){
                case 'body':
                    $body = LocalBody::findorfail($article_id);
                    $article_name = $body->translation('en')->first()->name;
                    break;
                case 'district':
                    $district = District::findorfail($article_id);
                    $article_name = $district->translation('en')->first()->name;
                    break;
                case 'province':
                     $province = Province::findorfail($article_id);
                     $article_name = $province->translation('en')->first()->name;
                     break;
            }

            $suggestion->setAttribute('article_name', $article_name);

        }

        return view('admin.pages.index')->with( 'suggestions',$suggestions );
    }

    public function detail($id){
        $suggestion = Suggestion::findOrFail($id);

        return view('admin.pages.suggestion_details')->with('suggestion',$suggestion);
    }

    public function delete($id){
         Suggestion::findOrFail($id)->delete();

        return Redirect::to( url('/lgisadminpanel'));
    }

    public function login(){
        return view('admin.pages.login');
    }

    public function postLogin(Request $request){

        if(Auth::attempt(['email' => $request->get('email') , 'password' => $request->get('password')]))
        {
            return Redirect::to('/lgisadminpanel');
        } else {
            return Redirect::to('lgisadminpanel/login')
                ->withErrors("Email and Password Do not Match")
                ->withInput();
        }
    }

    public function getLogout(){
        Auth::logout();
        return Redirect::to('lgisadminpanel/login');
    }

    function paginate($items, $perPage)
    {
        $pageStart = request('page', 1);
        $offSet    = ($pageStart * $perPage) - $perPage;
        $itemsForCurrentPage = $items->slice($offSet, $perPage);

        return new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsForCurrentPage, $items->count(), $perPage,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }


}
