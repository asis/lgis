<?php

namespace App\Http\Controllers;


use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Province;
use App\ProvinceTranslation;
use App\Body;
use File;
use function MongoDB\BSON\fromJSON;
use Response;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $locale
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        \App::setLocale($locale);
        //$province_translations = ProvinceTranslation::where('language', 'en')->get();;

        $provinces = Province::all();

        if (\Request::is('api/*')) {
            $response = array();
            foreach ($provinces as $province) {
                array_push($response, array(
                        'province_id' => $province->id,
                        'province_name' => $province->translation()->first()->name,
                        'count_district' => count($province->districts),
                        'count_bodies' => count($province->bodies),
                        'count_wards' => $province->body->sum('ward_no'),
                        'province_info' => $province->translation()->first()->description,
                    )
                );
            }
            return response()->json($response);
        }
        return view('app.province.index')->with('provinces',$provinces);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  Locale $locale
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        \App::setLocale($locale);

        $province = Province::findOrFail($id);

        $districts = $province->districts;

        if ( \Request::is('api/*')) {

            $district_body_array = array();

            foreach ($districts as $d) {

                $d['name'] = $d->translation()->first()->name;
                $d['count_wards'] = $d->bodies->sum('ward_no');
                $d->bodies;
                array_push($district_body_array, array(
                        'district' => $d,
                    )
                );
            }

            $province_details = array();
            array_push($province_details, array(
                    'province_id' => $province->id,
                    'province_name' => $province->translation()->first()->name,
                    'province_description' => $province->translation()->first()->description,
                    'count_district' => count($province->districts),
                    'count_bodies' => count($province->bodies),
                    'count_wards' => $province->body->sum('ward_no'),
                    'data' => $district_body_array
                )
            );
            return response()->json($province_details);
        }


        return view('app.province.details')->with('province',$province);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

//        $data = [ "type" => "FeatureCollection",
//                "features" => [
//                    [
//                        "type" =>"Feature",
//                        "geometry" => [
//                            "type"=> "Polygon",
//                            "coordinates" => ""
//                        ],
//                        "properties" => ["prop0" => "value0"]
//                    ],
//                    [
//                        "type" =>"Feature",
//                        "geometry" => [
//                                "type"=> "Point",
//                            "coordinates" => [102.0, 0.5]
//                        ],
//                        "properties" => ["prop0" => "value0"]
//                    ]
//                ]
//        ];

    public function createGeoJSON($locale)
    {

        \App::setLocale($locale);

        $provinces = Province::all();

        $features = array();

        foreach ($provinces as $province) {

            $lat_long = $province->provinceShapeFile->lat_lng;

            $lat_long = explode(";", $lat_long);

            $lat_long_array = array();
            foreach ($lat_long as $ll) {

                $int_ll = explode(",", $ll);

                array_push($lat_long_array, [(double)$int_ll[0], (double)$int_ll[1]]);
            }

            array_push($features, array(
                    'type' => "Feature",
                    "geometry" => [
                        "type" => "Polygon",
                        "coordinates" => [$lat_long_array]
                    ],
                    "properties" => [
                        "name" => $province->translation()->first()->name,
                        "color" => $province->provinceShapeFile->color,
                        "label" => $province->id,
                        'metro' => count($province->body->where('type', 'metro')),
                        'sub_metro' => count($province->body->where('type', 'sub_metro')),
                        'muni' => count($province->body->where('type', 'municipality')),
                        'rural' => count($province->body->where('type', 'rural_municipality')),
                        'ward' => $province->body->sum('ward')

                    ]
                )
            );
        }

        $filename = "provinces.json";

        $data = json_encode(["type" => "FeatureCollection",
            "features" => $features
        ]);
        File::put(public_path('js/geoJSON/' . $filename), $data);

        return response()->json(['status' => "success", "message" => "New provine File is created. Before using it please reduce the size from http://mapshaper.org"]);

//        return response()->json(
//                            ["type" => "FeatureCollection",
//                                "features" => $features
//                            ]
//            );
    }

    public function getGeoJSON($locale)
    {
        $filename = "provinces.json";
        return Response::download(public_path('js/geoJSON/' . $filename));
    }

    public function areaOfDistricts($id){

        if ( \Request::is('api/*')) {

            $province = Province::findOrFail($id);
            $districts = $province->districts;

            $data = array();

            foreach ($districts as $district) {
                array_push($data, array(
                    'name' => $district->translation('en')->first()->name,
                    'area' => (double) $district->translation('en')->first()->area
                ));
            }

            return response()->json($data);
        }
    }

    public function populationOfDistricts($id){
        if ( \Request::is('api/*')) {
            $province = Province::findOrFail($id);
            $districts = $province->districts;
            $data = array();

            foreach ($districts as $district) {
                array_push($data, array(
                    'name' => $district->translation('en')->first()->name,
                    'population' => (double) $district->translation('en')->first()->population
                ));
            }
            return response()->json($data);
        }
    }

    public function populationOfMaleAndFemale($id){
        if ( \Request::is('api/*')) {
            $province = Province::findOrFail($id);

            $data = array();
            array_push($data,[
                'name' => 'Male Population',
                'population' => (double) $province->translation('en')->first()->male_population
            ]);

            array_push($data,[
                'name' => 'Female Population',
                'population' => (double) $province->translation('en')->first()->female_population
            ]);

            return response()->json($data);
        }
    }

    public function populationOfProvinceAndNepal($id){
        if ( \Request::is('api/*')) {
            $province = Province::findOrFail($id);

            $nepal = DB::table('nepal')->first();

            $data = array();
            array_push($data,[
                'name' => 'Population of Nepal',
                'population' => (double) $nepal->population,
            ]);

            array_push($data,[
                'name' => 'Population of Province',
                'population' => (double) $province->translation('en')->first()->population
            ]);

            return response()->json($data);
        }
    }

    public function areaOfProvinceAndNepal($id){
        if ( \Request::is('api/*')) {
            $province = Province::findOrFail($id);

            $nepal = DB::table('nepal')->first();

            $data = array();
            array_push($data,[
                'name' => 'Area of Nepal',
                'area' => (double) $nepal->area
            ]);

            array_push($data,[
                'name' => 'Area of Province',
                'area' => (double) $province->translation('en')->first()->area
            ]);

            return response()->json($data);
        }
    }
}
