<?php

namespace App\Http\Controllers;

use App\LocalBodyTranslation;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\LocalBody;
use App\Suggestion;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class NepalController extends Controller
{

    public function index($locale){

        \App::setLocale($locale);

        $body = LocalBody::all();

        $metro = count($body->where('type','metro'));
        $sub_metro = count($body->where('type','sub_metro'));
        $muni = count($body->where('type','municipality'));
        $rural_muni = count($body->where('type','rural_municipality'));
        $local_bodies = count($body);
        $ward = $body->sum('ward_no');
        $population = $body->sum('ward_no');

        $wikipedia = new \Casinelli\Wikipedia\Wikipedia;
        $scraping = $wikipedia->search("Nepal")->getSentences(6);

        return view('app.index')->with(['metro'=>$metro,
            'sub_metro'=>$sub_metro,
            'muni'=>$muni,
            'rural_muni'=>$rural_muni,
            'ward'=>$ward,
            'local_bodies' => $local_bodies,
            'scrap' => $scraping
        ]);
    }

    public function getMetro($locale){
        \App::setLocale($locale);

        $body = LocalBody::where('type','metro')->get();

        $body = $body->sort(function($a, $b)
        {
            $a = $a->translation()->first()->name;
            $b = $b->translation()->first()->name;
            //here you can do more complex comparisons
            //when dealing with sub-objects and child models
            if ($a === $b) {
                return 0;
            }
            return ($a > $b) ? 1 : -1;
        });

        $body = $this::paginate($body, 5);

        return view('app.body.index')->with('bodies',$body);
    }

    public function getSubMetro($locale){
        \App::setLocale($locale);

        $body = LocalBody::where('type','sub_metro')->get();

        $body = $body->sort(function($a, $b)
        {
            $a = $a->translation()->first()->name;
            $b = $b->translation()->first()->name;
            //here you can do more complex comparisons
            //when dealing with sub-objects and child models
            if ($a === $b) {
                return 0;
            }
            return ($a > $b) ? 1 : -1;
        });

        $body = $this::paginate($body, 5);

        return view('app.body.index')->with('bodies',$body);
    }

    public function getMunic($locale){
        \App::setLocale($locale);

        $body = LocalBody::where('type','municipality')->get();

        $body = $body->sort(function($a, $b)
        {
            $a = $a->translation()->first()->name;
            $b = $b->translation()->first()->name;
            //here you can do more complex comparisons
            //when dealing with sub-objects and child models
            if ($a === $b) {
                return 0;
            }
            return ($a > $b) ? 1 : -1;
        });

        $body = $this::paginate($body, 5);

        return view('app.body.index')->with('bodies',$body);
    }

    public function getRuralMuni($locale){
        \App::setLocale($locale);

        $body = LocalBody::where('type','rural_municipality')->get();

        $body = $body->sort(function($a, $b)
        {
            $a = $a->translation()->first()->name;
            $b = $b->translation()->first()->name;
            //here you can do more complex comparisons
            //when dealing with sub-objects and child models
            if ($a === $b) {
                return 0;
            }
            return ($a > $b) ? 1 : -1;
        });

        $body = $this::paginate($body, 5);

        return view('app.body.index')->with('bodies',$body);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'description' => 'required',

            'type' => 'required',
            'article_id' => 'required'
        ]);

        $suggestion = new Suggestion();

        $suggestion->name= $request['name'];
        $suggestion->address= $request['address'];
        $suggestion->email= $request['email'];
        $suggestion->content= $request['description'];

        $suggestion->type= $request['type'];
        $suggestion->article_id= $request['article_id'];

        $suggestion->save();

        \Session::flash('message', 'Task successfully Edited!');

        return redirect()->back();
    }

    public function search(){

        // Retrieve the user's input and escape it
        $query = e(Input::get('q',''));

        // If the input is empty, return an error response
        if(!$query && $query == '') return Response::json(array(), 400);

        $body = LocalBodyTranslation::where('name','like','%'.$query.'%')
            ->orderBy('name','asc')
            ->take(5)
            ->get(array('local_body_id','name'))->toArray();

	    //$categories = ...

        // Normalize data
        // Add type of data to each item of each set of results
        // Merge all data into one array
        $data = array_merge($body);

        return Response::json(array(
            'data'=>$data
        ));
}

    public function livesearch(Request $request){
        $output = "";
        $body = "";
//        if ($request -> ajax()){
            $body = LocalBodyTranslation::where('name','like','%'. $request -> search .'%')
                ->orderBy('name','asc')
                ->take(5)
                ->get();
//        }
        if ($body) {
            $output_name = array();
            $output_id = array();

            foreach ($body as $body_name) {
                $output_name[] = $body_name->name;
                array_push($output_id, $body_name->id);
            }
        }

        return Response(array(
            $output_name,
            $output_id
        ));
    }

    function paginate($items, $perPage)
    {
        $pageStart = request('page', 1);
        $offSet    = ($pageStart * $perPage) - $perPage;
        $itemsForCurrentPage = $items->slice($offSet, $perPage);

        return new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsForCurrentPage, $items->count(), $perPage,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }

//    function id_manager(){
//
//        $local_bodys = LocalBodyTranslation::where('language','ne')->get();
//        //$representatives = Representative::pluck('name','id');
//        foreach ($local_bodys as $local_body){
//            $lname = $local_body['name'];
//            $rep = Representative::where('name','like','%'.$lname.'%')->update(['id'=>$local_body['local_body_id']]);
//        }
//    }

//    function id_manager(){
//
////        $local_bodys = LocalBodyTranslation::where('language','ne')->get();
//
//        $data = DB::table('temp')->get();
//
//        //$representatives = Representative::pluck('name','id');
//
//        $count = 0;
//
////        dd(count($data));
//        $name = array();
//
//        foreach ($data as $d){
//
//            //$rep = Representative::where('name','like','%'.$lname.'%')->update(['id'=>$local_body['local_body_id']]);
//
////            $local_body = LocalBodyTranslation::where('name','like','%'.$d->name.'%')->get();
//
//            $local_body = LocalBodyTranslation::where('name','like','%'.$d->name.'%')->first();
//
//
//            if ($local_body['local_body_id'] != null){
//                $local_body_edited = LocalBodyTranslation::where('local_body_id',$local_body->local_body_id)->first()
//                    ->update([
//                        'population' => $d->total,
//                        'area' => $d->area,
//                        'male_population' => $d->male,
//                        'female_population' => $d->female
//                    ]);
//            }else{
//
//                array_push($name,$d->name);
//
//                $count = $count + 1;
//            }
//
//        }
//
//        dd($name);
//    }

//    function id_manager(){
//        $local_body = LocalBodyTranslation::where('language','ne')->get();
//
//        foreach ($local_body as $body){
//            $lname = $body['name'];
//
//            $other = DB::table('other_information')
//                ->where('name','like','%'.$lname.'%')
//                ->update(['local_body_id' => $body['local_body_id']]);
//
//        }
//    }

}
