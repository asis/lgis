<?php

namespace App\Http\Controllers;

use App\LocalBody;
use Illuminate\Support\Facades\DB;
use App\Representative;
use Illuminate\Http\Request;
use App\District;
use Intervention\Image\Facades\Image;

use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;

class AdminBodyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $body = LocalBody::paginate(12);
//        dd($body);
        return view('admin.pages.body.index')->with('bodies', $body);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();

        return view('admin.pages.body.create')->with('districts', $districts);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'headquarter' => 'required',
            'male_population' => 'required',
            'female_population' => 'required',
            'area' => 'required',
            'density' => 'required',
            'mayor_name' => 'required',
            'mayor_email' => 'required',
            'mayor_phone' => 'required',
            'deputy_name' => 'required',
            'deputy_email' => 'required',
            'deputy_phone' => 'required',
//
////        'east_border' => 'required',
////        'west_border' => 'required',
////        'south_border' => 'required',
////        'north_border' => 'required',
//
            'name_ne' => 'required',
            'description_ne' => 'required',
            'headquarter_ne' => 'required',
            'population_ne' => 'required',
            'area_ne' => 'required',
            'density_ne' => 'required'
        ]);

//        $body = new LocalBody();
//        $body_translation = new LocalBodyTranslation();
        $representative = new Representative();

        //$province->fill($input)->save();
//        $body->district_id = $request->district_id;
//
//        $body->province_id = $district->province->id;
//        $body->type = $request->type;
//        $body->ward_no = $request->ward_no;

        if($request->hasFile('map_upload')){

            $png_url = strtolower($request['name'].".png");
            $path = public_path().'/images/bodies/' . $png_url;
            Image::make(file_get_contents($request->map_upload))->save($path);
        }

        $district = District::findOrfail($request->district_id);

        $body = DB::table('local_bodies')->insert([
            'province_id' => $request->district_id,
            'district_id' => $district->province->id,
            'type' => $request->type,
            'ward_no' => $request->ward_no,
        ]);

        $population = (double)male_population + (double)female_population;

        $body_translation_en = DB::table('local_bodies_translations')->insert([
            'local_body_id' => $body->id,
            'language' => "en",
            'is_default' => "1",
            'name' => $request->name,
            'description' => $request->description,
            'headquarter' => $request->headquarter,
            'male_population' => $request->male_population,
            'female_population' => $request->female_population,
            'population' => $request->$population,
            'area' => $request->$request->area,
            'density' => $request->$request->density,
            'ward_mix_of' => "",
        ]);

        $body_translation_ne = DB::table('local_bodies_translations')->insert([
            'local_body_id' => $body->id,
            'language' => "ne",
            'is_default' => "0",
            'name' => $request->name_ne,
            'description' => $request->description_ne,
            'headquarter' => $request->headquarter,
            'male_population' => $request->male_population,
            'female_population' => $request->female_population,
            'population' => $request->$population,
            'area' => $request->$request->area,
            'density' => $request->$request->density,
            'ward_mix_of' => "",]);
         $population = (string)((double)$request->male_population + (double) $request->female_population);

        $body = LocalBody::orderBy('id', 'desc')->first();

        $body_translation_en = DB::table('local_body_translations')->insert([
            'local_body_id'=> $body->id,
            'language'=>"en",
            'is_default'=>"1",
            'name'=>$request->name,
            'description'=>$request->description,
            'headquarter'=>$request->headquarter,
            'male_population'=>$request->male_population,
            'female_population'=>$request->female_population,
            'population'=>$population,
            'area'=>$request->area,
            'density'=>$request->density,
            'border' => '',
            'ward_mix_of'=>"",
        ]);

        $body_translation_ne = DB::table('local_body_translations')->insert([
            'local_body_id'=>$body->id,
            'language'=>"ne",
            'is_default'=>"0",
            'name'=>$request->name_ne,
            'description'=>$request->description_ne,
            'headquarter'=>$request->headquarter_ne,
            'male_population'=>$request->male_population,
            'female_population'=>$request->female_population,
            'population'=>$population,
            'area'=>$request->area,
            'density'=>$request->density,
            'border' => '',
            'ward_mix_of'=>"",
        ]);

        $representative->local_body_id = $body->id;
        $representative->mayor_name = $request['mayor_name'];
        $representative->mayor_email = $request['mayor_email'];
        $representative->mayor_phone = $request['mayor_phone'];
//        mayor_pic

        $representative->deputy_name = $request['deputy_mayor_name'];
        $representative->deputy_email = $request['deputy_mayor_email'];
        $representative->deputy_phone = $request['deputy_mayor_phone'];
//        deputy_pic

        if($request->hasFile('mayor_photo')){

            $png_url = "mayor-".time().".png";
            $path = public_path().'/images/representatives/' . $png_url;
            Image::make(file_get_contents($request->mayor_photo))->save($path);

            $representative->mayor_pic = url('/images/representatives/'. $png_url);
        }

        if($request->hasFile('deputy_mayor_photo')){

            $png_url = "deputy-".time().".png";
            $path = public_path().'/images/representatives/' . $png_url;
            Image::make(file_get_contents($request->deputy_mayor_photo))->save($path);

            $representative->deputy_pic = url('/images/representatives/'. $png_url);
        }

        $representative->save();


//        dd('here');


//        $body->save();
//        dd($body->id);

        \Session::flash('message', 'Body successfully Created!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $body = LocalBody::findOrFail($id);

        return view('admin.pages.body.show')->with('bodies', $body);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $body = LocalBody::findOrFail($id);
//        dd($body);
        return view('admin.pages.body.edit')->with('body', $body);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $body = LocalBody::findOrFail($id);
//        $this->validate($request, [
//            'name' => 'required',
//            'description' => 'required',
//            'headquarter' => 'required',
//            'male_population' => 'required',
////            'population' => 'required',
//            'female_population' => 'required',
//            'area' => 'required',
//            'density' => 'required',
//            'mayor_name' => 'required',
//            'mayor_email' => 'required',
//            'mayor_phone' => 'required',
//            'deputy_name' => 'required',
//            'deputy_email' => 'required',
//            'deputy_phone' => 'required',
////
//////        'east_border' => 'required',
//////        'west_border' => 'required',
//////        'south_border' => 'required',
//////        'north_border' => 'required',
////
////            'name_ne' => 'required',
////            'description_ne' => 'required',
////            'headquarter_ne' => 'required',
////            'population_ne' => 'required',
////            'area_ne' => 'required',
////            'density_ne' => 'required'
//        ]);
        $input = $request->all();
//        dd($input);

        if($request->hasFile('map_upload')){

            $png_url = strtolower($request['name'].".png");
            $path = public_path().'/images/bodies/' . $png_url;
            Image::make(file_get_contents($request->map_upload))->save($path);
        }

        $body->translation('en')->first()->fill($input)->save();
        $input['name'] = $request['name_ne'];
        $input['description'] = $request['description_ne'];
        $input['headquarter'] = $request['headquarter_ne'];

        $body->translation('ne')->first()->fill($input)->save();


        $representative = $body->representative;

        if ($representative == null) {

            $representative = new Representative();
        }

        $representative->local_body_id = $body->id;
        $representative->mayor_name = $request['mayor_name'];
        $representative->mayor_email = $request['mayor_email'];
        $representative->mayor_phone = $request['mayor_phone'];

        $representative->deputy_name = $request['deputy_mayor_name'];
        $representative->deputy_email = $request['deputy_mayor_email'];
        $representative->deputy_phone = $request['deputy_mayor_phone'];

        if($request->hasFile('mayor_photo')){

            $png_url = "mayor-".time().".png";
            $path = public_path().'/images/representatives/' . $png_url;
            Image::make(file_get_contents($request->mayor_photo))->save($path);

            $representative->mayor_pic = url('/images/representatives/'. $png_url);
        }

        if($request->hasFile('deputy_mayor_photo')){

            $png_url = "deputy-".time().".png";
            $path = public_path().'/images/representatives/' . $png_url;
            Image::make(file_get_contents($request->deputy_mayor_photo))->save($path);

            $representative->deputy_pic = url('/images/representatives/'. $png_url);
        }

        $representative->save();

        \Session::flash('message', 'Task successfully Edited!');

        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_data_from_web($id)
    {
        $body = LocalBody::findOrFail($id);
//        dd($body);
        $representative = $body->representative;
        if ($representative == null) {

            $representative = new Representative();

            $representative->local_body_id = $body->id;

            $representative->mayor_name = "emp";
            $representative->mayor_email = "emp";
            $representative->mayor_phone = "emp";
            $representative->deputy_name = "emp";
            $representative->deputy_email = "emp";
            $representative->deputy_phone = "emp";

            $unit_fullname = $body->translation('en')->first()->name;
            $unit_firstname = explode(' ', trim($unit_fullname));
            $url = "http://$unit_firstname[0]mun.gov.np/";

            $representative->website = $url;

            $representative->save();

        }
        $url = $representative->website;

        $client = new Client();
        try{
            $crawlers = $client->request('GET', $url);
        }
        catch (\Exception $e){
            return \Response::view('error');
        }

        $unit_intro_ne = $crawlers->filter('#block-system-main > div > div > div > .field-type-text-with-summary')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        $url = $representative->website;
        $url_en = $url . "en";
        $client = new Client();
        try {
            $crawlers = $client->request('GET', $url_en);
        }
        catch (\Exception $e){
            return \Response::view('error');
        }

        $unit_intro_en = $crawlers->filter('#block-system-main > div')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        foreach ($unit_intro_en as $unit) {
            $unit_intro_en[] = preg_replace('~[\r\n\t]+~', '', $unit);
        }

        foreach ($unit_intro_en as $key => $one) {
            if (strpos($one, 'Copyright ©') !== false)
                unset($unit_intro_en[$key]);
            array_push($unit_intro_en, "No data");
        }
        foreach ($unit_intro_en as $key => $one) {
            if (strpos($one, 'No front page content has been created yet') !== false)
                unset($unit_intro_en[$key]);
            array_push($unit_intro_en, "No data");
        }
        foreach ($unit_intro_en as $key => $one) {
            if (strpos($one, '  ') !== false)
                unset($unit_intro_en[$key]);
            array_push($unit_intro_en, "No data");
        }
        foreach ($unit_intro_en as $key => $one) {
            if (strpos($one, 'The requested page') !== false)
                unset($unit_intro_en[$key]);
            array_push($unit_intro_en, "No data");
        }
        $unit_intro_en = array_values($unit_intro_en);

        $url = $representative->website;
        $client = new Client();
        $crawlers = $client->request('GET', $url);

        $photos = $crawlers->filter('.views-field-field-photo > .field-content > a')->each(function (Crawler $node, $i) {
            return $node->html();
        });
        $photo = array();
        foreach ($photos as $node) {
            preg_match('/<img\s.*?\bsrc="(.*?)".*?>/si', $node, $photo[]);
        }
//        dd($photo);
        $representaive_name = $crawlers->filter('.region-featured > div > div > div > div > * > .views-field-title')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        $representaive_title = $crawlers->filter('.region-featured > div > div > div > div > * > .views-field-field-designation')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        $representaive_phone = $crawlers->filter('.region-featured > div > div > div > div > * > .views-field-field-phone')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        $representaive_email = $crawlers->filter('.region-featured > div > div > div > div > * > .views-field-field-email')->each(function (Crawler $node, $i) {
            return $node->text();
        });
//        dd($representaive_email);

        if (empty($representaive_name)){
            $representaive_name = $crawlers->filter('table > tbody > tr > td > .views-field-field-title')->each(function (Crawler $node, $i) {
                return $node->text();
            });
//            dd($representaive_name);
//            dd($representaive_title);
        }
        if (empty($representaive_name)){
            $representaive_name = $crawlers->filter('table > tbody > tr > td > .views-field-title')->each(function (Crawler $node, $i) {
                return $node->text();
            });
//            dd($representaive_name);
//            dd($representaive_title);
        }
        if (empty($representaive_name)){
            array_push($representaive_name,"not found");
            array_push($representaive_name,"not found");
        }
//        dd("out");
        if (empty($representaive_name[1])){
//            dd("in");
            $representaive_name[1] = "not found";
        }
//        dd("set");

        if (empty($representaive_title)){
        $representaive_title = $crawlers->filter('table > tbody > tr > td > .views-field-designation')->each(function (Crawler $node, $i) {
            return $node->text();
        });
        }

        if (empty($representaive_title)){
            $representaive_title = $crawlers->filter('table > tbody > tr > td > .views-field-field-designation')->each(function (Crawler $node, $i) {
                return $node->text();
            });
        }
        if(empty($representaive_title)){
            array_push($representaive_title,"not found");
            array_push($representaive_title,"not found");
        }
        if (empty($representaive_title[1])){
//            dd("in");
            $representaive_title[1] = "not found";
        }
//        dd("here");

        if (empty($photo)){
//            dd("in");
            $photos = $crawlers->filter('table > tbody > tr > td > .views-field-field-image')->each(function (Crawler $node, $i) {
                return $node->text();
            });
            $photo = array();
            foreach ($photos as $node) {
                preg_match('/<img\s.*?\bsrc="(.*?)".*?>/si', $node, $photo[]);
            }
//            dd($photo);
        }
        if (empty($photo)){
            $photo[0] = "not found";
            $photo[1] = "not found";
            if (empty($photo[0])){
                $photo[0][1] = "not found";
            }
            if (empty($photo[1])){
                $photo[1][1] = "not found";
            }
        }
        if (empty($photo[0])){
            $photo[0][0] = "not found";
            $photo[0][1] = "not found";

        }
        if (empty($photo[1])){
            $photo[1][0] = "not found";
            $photo[1][1] = "not found";
        }
        if (!isset($photo[1])){
//            dd("in");
            $photo[1][0] = "not found";
            $photo[1][1] = "not found";

        }
//        dd($photo);



        if (empty($representaive_phone)){
            $representaive_phone = $crawlers->filter('table > tbody > tr > td > .views-field-field-phone')->each(function (Crawler $node, $i) {
                return $node->text();
            });


        }
        if(empty($representaive_phone)){
            array_push($representaive_phone,"not found");
            array_push($representaive_phone,"not found");
        }

        if (empty($representaive_phone[1])){
//            dd("in");
            $representaive_phone[1] = "not found";
        }

//        dd($representaive_email);

        if (empty($representaive_email)){
            $representaive_email = $crawlers->filter('table > tbody > tr > td > .views-field-field-email')->each(function (Crawler $node, $i) {
                return $node->text();
            });
//            dd("here");
        }
//        dd($representaive_email);
        if(empty($representaive_email)){
//            dd("here");
            array_push($representaive_email,"not found");
            array_push($representaive_email,"not found");
        }
//        dd($representaive_email);
        if (empty($representaive_email[1])){
//            dd("in");
            $representaive_email[1] = "not found";
        }


        if (empty($unit_intro_ne)){
            array_push($unit_intro_ne,"not found");
        }
        if (empty($unit_intro_en)){
            array_push($unit_intro_en,"not found");
        }
//        dd($representaive_title);
//        dd($representative->website);
//        $representative->mayor_name = $representaive_name[0];
//        $representative->mayor_email = $representaive_email[0];
//        $representative->mayor_phone = $representaive_phone[0];
//        $representative->mayor_pic = $photo[0][1];
//        $representative->deputy_name = $representaive_name[1];
//        $representative->deputy_email = $representaive_email[1];
//        $representative->deputy_phone = $representaive_phone[1];
//        $representative->deputy_pic = $photo[1][1];
//
//        $representative->save();
//        dd($representaive_email);

        dd($photo);

        return view('admin.pages.body.edit')->with(
            array(
                'ne_body_info' => $unit_intro_ne,
                'en_body_info' => $unit_intro_en,
                'representative_image' => $photo,
                '$representative_title' => $representaive_title,
                'representative_name' => $representaive_name,
                'representative_email' => $representaive_email,
                'representative_phone' => $representaive_phone,
                'body' => $body
            ));
    }


    public function getOtherInformationForm($id){

        $information = DB::table('other_information')->where('local_body_id',$id)->first();

        $data = array(
            'information'=> $information,
            'body_id' => $id
        );

        return view('admin.pages.body.other_info_edit')->with($data);
    }

    public function postOtherInformationForm(Request $request,$id){

        $information = DB::table('other_information')->where('local_body_id',$id)->first();

        if($information == null){
            DB::table('other_information')
                ->insert([
                    'local_body_id' => $id,
                    'hospital' => $request->hospital,
                    'health_centre' => $request->health_centre,
                    'health_post' => $request->health_post,
                    'sub_health_post' => $request->sub_health_post,
                    'ayurvedic_medicine_centre' => $request->ayurvedic_medicine_centre,
                    'rural_health_social_volunteer' => $request->rural_health_social_volunteer,
                    'child_care_centre' => $request->child_care_centre,
                    'primary_school' => $request->primary_school,
                    'secondary_school' => $request->secondary_school,
                    'college' => $request->college,
                    'university' => $request->university,
                    'banijya_bank' => $request->banijya_bank,
                    'development_bank' => $request->development_bank,
                    'financial_institution' => $request->financial_institution,
                    'laghu_bittiya_sanstha_ltd' => $request->laghu_bittiya_sanstha_ltd,
                    'saving_and_credit_cooperative_ltd' => $request->saving_and_credit_cooperative_ltd,
                    'pitch_road' => $request->pitch_road,
                    'semi_pitch_road' => $request->semi_pitch_road,
                    'rough_road' => $request->rough_road,
                    'driking_water_available' => $request->driking_water_available,
                    'public_toilet_available' => $request->public_toilet_available,
                    'center_for_media' => $request->center_for_media,
                    'micro_hydro' => $request->micro_hydro,
                    'solar' => $request->solar,
                    'bio_gas' => $request->bio_gas,
                    'improved_stove' => $request->improved_stove,
                ]);
        }else{
            DB::table('other_information')
                ->where('local_body_id',$id)
                ->update([
                    'hospital' => $request->hospital,
                    'health_centre' => $request->health_centre,
                    'health_post' => $request->health_post,
                    'sub_health_post' => $request->sub_health_post,
                    'ayurvedic_medicine_centre' => $request->ayurvedic_medicine_centre,
                    'rural_health_social_volunteer' => $request->rural_health_social_volunteer,
                    'child_care_centre' => $request->child_care_centre,
                    'primary_school' => $request->primary_school,
                    'secondary_school' => $request->secondary_school,
                    'college' => $request->college,
                    'university' => $request->university,
                    'banijya_bank' => $request->banijya_bank,
                    'development_bank' => $request->development_bank,
                    'financial_institution' => $request->financial_institution,
                    'laghu_bittiya_sanstha_ltd' => $request->laghu_bittiya_sanstha_ltd,
                    'saving_and_credit_cooperative_ltd' => $request->saving_and_credit_cooperative_ltd,
                    'pitch_road' => $request->pitch_road,
                    'semi_pitch_road' => $request->semi_pitch_road,
                    'rough_road' => $request->rough_road,
                    'driking_water_available' => $request->driking_water_available,
                    'public_toilet_available' => $request->public_toilet_available,
                    'center_for_media' => $request->center_for_media,
                    'micro_hydro' => $request->micro_hydro,
                    'solar' => $request->solar,
                    'bio_gas' => $request->bio_gas,
                    'improved_stove' => $request->improved_stove,
                ]);
        }

        \Session::flash('message', 'Task successfully Edited!');

        return redirect()->back();
    }
}
