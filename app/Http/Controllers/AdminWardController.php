<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ward;
use Illuminate\Support\Facades\Redirect;

class AdminWardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('admin.pages.ward.create')->with('body_id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ward = new Ward();

        $ward->name = $request->name;
        $ward->population = $request->population;
        $ward->area = $request->area;
        $ward->ward_id = $request->ward_id;
        $ward->local_body_id = $request->body_id;

        $ward->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ward = Ward::findOrFail($id);
        return view('admin.pages.ward.edit')->with('ward',$ward);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ward = Ward::findOrFail($id);
        Ward::where('id', $id)->update([
            'name' => $request['name'],
            'population' => $request['population'],
            'area' => $request['area']
            ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
