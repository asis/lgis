<?php

return [

    //App
    'title' => 'Local Governance Information System',
    'name' => 'LGIS',


    //ToolBar
    'language' => 'भाषा छनोट गर्नुहोस',
    'detail_view'=> 'Detail View',
    'help' => 'Help',
    'about' => 'About us',
    'contact' => 'Contact us',

    'province' => 'Provinces',
    'district' => 'Districts',
    'body' => 'Local Units',
    'ward' => 'Wards',

    'search' => 'Search for Rural Municipality, Municipality and more',


    // Index.php descriptions
    'nepal' => 'Nepal',
    'introduction' =>'Introduction',
    'description'=>'Description',

    //Card-box view showing Information
    'metro' => 'Metropolitan City',
    'sub_metro' => 'Sub-Metropolitan City',
    'munic' => 'Municipality',
    'rural_munic' => 'Rural Municipality',
    'wards' => 'wards',
    'population' => 'Population',
    'more' => 'More',

    'ofice_of_rural_munic' => 'Office of the Rural Municipal Executive',

    'map' => 'Map',

    //other english
    'graphical_representation' => 'Graphical Representation',
    'people_representative' => 'People\'s Representative',
    'mayor' => 'Mayor',
    'deputy_mayor' => 'Deputy-Mayor',
    'area' => 'Area',

    'total_area' => 'Total Area of ',
    'in_respect_to_district' => 'in respect to District:',

    'population_of_every'=> 'Population of Every',
    'area_of_every' => 'Area of Every',
    'of_every' => 'of Every ',

    'other' => 'Other informations',

    //footer
    'copyright' => 'Copyright',
    'all_right_reserved' => 'All right reserved',
    'fullstop' => '.',
    'year' => '2014'
];