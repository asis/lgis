<?php

return [

    //App
    'title' => 'स्थानीय प्रशासन सूचना प्रणाली',

    'name' => 'स्.प्र.सू.प्र',


    //ToolBar
    'language' => 'Select Your Language',
    'detail_view'=> 'विस्तृत',
    'help' => '	सहयोग',
    'about' => 'हाम्रो बारेमा',
    'contact' => 'हामीलाई सम्पर्क गर्नुहोस',

    'province' => 'प्रदेशहरू',
    'district' => 'जिल्लाहरू',
    'body' => 'स्थानीय निकायहरू',
    'ward' => 'वडाहरू',

    'search' => 'गाउँपालिका, नगरपालिका र थप को लागि खोज',

    // Index.php descriptions
    'nepal' => 'नेपाल',
    'introduction' => 'परिचय',
    'description'=>'विवरण',

    //Card-box view showing Information
    'metro' => 'महानगरपालिका',
    'sub_metro' => 'उप-महानगरपालिका',
    'munic' => 'नगरपालिका',
    'rural_munic' => 'गाउँपालिका',
    'wards' => 'वडा',
    'population' =>'जनसंख्या',
    'more'=> 'थप',

    'map' => 'नक्शा',

    //other nepali
    'graphical_representation' => 'चित्रमय प्रतिनिधित्व',
    'people_representative' => 'जन प्रतिनिधि',
    'mayor' => 'प्रमुख',
    'deputy_mayor' => 'उपप्रमुख',
    'area' => 'क्षेत्र',
    'density' => 'घनत्व',

    'total_area' => 'जम्मा क्षेत्र',
    'in_respect_to_district' => 'स्थानीयतहको आधारमा',
    'of_every' => 'का आदरमा',

    'population_of_every'=> 'जम्मा जनसंख्या',
    'area_of_every' => 'जम्मा क्षेत्र',

    'other' => 'अन्य जानकारी',


    //footer
    'copyright' => 'प्रतिलिपि अधिकार',
    'all_right_reserved' => 'सबै अधिकार सुरक्षित',
    'fullstop' => '।',
    'year' => '२०७४'
];