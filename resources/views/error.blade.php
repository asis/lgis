
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>LGIS</title>



    <link media="all" type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">


    <!-- Bootstrap -->


    <link media="all" type="text/css" rel="stylesheet" href="{{ url('/css/bootstrap.min.css')}}">

    <link media="all" type="text/css" rel="stylesheet" href="{{ url('/css/style.css')}}">


    <link media="all" type="text/css" rel="stylesheet" href="{{ url('/selectize/css/selectize.bootstrap3.css')}}">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <script src="//oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->






</head>
<body>

<!-- This is side bar for menu -->
<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #2874f0; padding-top: 10px;">
        <span class="sidenav-heading"><a href="http://lgis.dev">Home</a></span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    </div>
    <a data-toggle="collapse" href="#collapse">Detail view</a>

    <div id="collapse" class="panel-collapse collapse">
        <a href="{{ url('/province')}}">Province</a>
        <a href="{{ url('/district')}}">Districts</a>
        <a href="{{ url('/body')}}">Bodies</a>
    </div>

    <a href="{{ url('/help')}}">Help</a>
    <a href="{{ url('/about')}}">About us</a>
    <a href="{{ url('/contact')}}">Contact us</a>

</div>

<!-- Hover -->
<div id="hover" style="font-family:Fontasy Himali"></div>

<!-- Navigation Bar -->
<div id="lgis-navbar">
    <div class="container">
        <div class="row row1">
            <ul class="largenav pull-right">
                <li class="upper-links dropdown"><a class="links" href="#">भाषा छनोट गर्नुहोस</a>
                    <ul class="dropdown-menu">
                        <li class="profile-li"><a class="profile-links" href="{{ url('/ne/awdw')}}">नेपाली</a></li>
                    </ul>
                </li>
                <li class="upper-links dropdown"><a class="links" href="#">Detail View</a>
                    <ul class="dropdown-menu">
                        <li class="profile-li"><a class="profile-links" href="{{ url('/en/province')}}">Provinces</a></li>
                        <li class="profile-li"><a class="profile-links" href="{{ url('/en/district')}}">Districts</a></li>
                        <li class="profile-li"><a class="profile-links" href="{{ url('/en/body')}}">Local Units</a></li>
                    </ul>
                </li>
                <li class="upper-links"><a class="links" href="{{ url('/en/help')}}">Help</a></li>
                <li class="upper-links"><a class="links" href="{{ url('/en/about')}}">About us</a></li>
                <li class="upper-links"><a class="links" href="{{ url('/en/contact')}}">Contact us</a></li>
            </ul>
        </div>
        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ LGIS</span></h2>
                <h1 style="margin:0px;"><span class="largenav"><a href="{{ url('/en')}}">LGIS</a></span></h1>
            </div>
            <div class="lgis-navbar-search smallsearch col-sm-8 col-xs-11">
                <div class="row">




                    <input class="lgis-navbar-input col-xs-11" placeholder="Search for Rural Municipality, Municipality and more" id="searchbox" type="text" list="livesearch" />
                    <datalist id="livesearch">
                    </datalist>



                    <button id = "searchbutton" class="lgis-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <!-- Svg of the  Search Image -->
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>

                </div>
            </div>
        </div>
    </div>
</div> <!-- end of nav bar -->

<input type="hidden" id="locale" value="en" />


<div class="container">
    404

    <ul>
        <li><a href="http://lgis.dev">Home</a></li>
    </ul>
</div>


<!-- Footer -->
<div id="footer">
    <div class="container-fluid" style="{width: 100%;}">
        <p class="text-center"> Copyright © LGIS 2014. All right reserved. </p>
        <span class="pull-right">Powered by <a href="#"> OUR COMPANY</a></span>
    </div>
</div>
<script type="text/javascript">
    var root = '{{ url('/') }}';
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>


<script src="{{ url('/selectize/js/standalone/selectize.min.js')}}"></script>


<script src="{{ url('/js/bootstrap.min.js')}}"></script>

<script src="{{ url('/js/navbar.js')}}"></script>


<script src="{{ url('/js/search.js')}}"></script>


</body>
</html>