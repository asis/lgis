@extends('app.master')

@section('breadcrumb')
    <!-- BreadCrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ url('/'. App::getLocale().'/province') }}">@lang('messages.province')</a></li>
            <li><a href="{{ url('/'. App::getLocale().'/province/'. $body->province->id ) }}">{{ $body->province->translation()->first()->name }}</a></li>
            <li><a href="{{ url('/'. App::getLocale().'/district') }}">@lang('messages.district')</a></li>
            <li><a href="{{ url('/'. App::getLocale().'/district/'. $body->district->id ) }}">{{ $body->district->translation()->first()->name }}</a></li>
            <li><a href="{{ url('/'. App::getLocale().'/body') }}">@lang('messages.body')</a></li>
            <li class="active">{{ $body->translation()->first()->name }}</li>
        </ol>
    </div>
@endsection

@section('content')

    <!-- The Main Container -->
    <div class="container" id="main-container">

        <div>
            <div>
                <div id="titlePosition">
                    <span> <h3>@lang('messages.body') - {{ $body->translation()->first()->name }} </h3></span>
                </div>

                <div id="editButton">

                    {{--<img src="{{ URL::asset('images/edit.png')}}" alt="Edit" height="32px" width="32px"/>--}}
                    <button id="myBtn" class="edtbtn">
                        <img src="{{ URL::asset('images/edit.png')}}" alt="Edit" height="32px" width="32px"/>
                    </button>

                </div>
            </div>

            <hr/>

            <div class="row box">
                <div class="col-md-6">
                    <h4><span> @lang('messages.introduction') : </span></h4>
                    <hr />
                    <p>{{ $body->translation()->first()->description }}</p>

                    <p>
                        Link:
                        <a href="{{ $body->representative->website or $body->translation()->first()->name . 'unit.gov.np' }}">
                            {{ $body->representative->website or $body->translation()->first()->name . "unit.gov.np" }}
                        </a>

                    </p>
                </div>
                <div class="col-md-6">
                    <h4>@lang('messages.map') : </h4>
                    <hr />
                    {{--<div id="map" class="box"></div>--}}

                    <div class="box img-container">

                        <img src="{{ URL::asset('images/bodies/' . $body->translation('en')->first()->name . '.png')}}" alt=" {{ $body->translation()->first()->name }}" >

                    </div>

                </div>
            </div>

            <hr style="margin-top: 20px;">

            <div class="row box">
                <h4><span>@lang('messages.people_representative')</span></h4>
                <hr/>
                <div class="col-md-6">
                    <div class="col-md-4 box">
                        <img src="{{$body->representative->mayor_pic or "Not Available"}}" style="height: auto; width: 150px">
                    </div>
                    <div class="col-md-8">
                        <h3>@lang('messages.mayor') :</h3>
                        <hr>
                        Name:  {{ $body->representative->mayor_name or "Not Available" }} <br />
                        Email: {{ $body->representative->mayor_email or "Not Available"}} <br/>
                        Phone number: {{ $body->representative->mayor_phone or "Not Available" }} <br/>
                    </div>



                </div>
                <div class="col-md-6">
                    <div class="col-md-4 box">
                        <img src="{{$body->representative->deputy_pic or "Not Available"}} " style="height: auto; width: 150px">
                    </div>
                    <div class="col-md-8">
                        <h3>@lang('messages.deputy_mayor')</h3>
                        <hr>

                        Name: {{ $body->representative->deputy_name  or "Not Available"}} <br />
                        Email: {{ $body->representative->deputy_email or "Not Available" }} <br />
                        Phone number: {{ $body->representative->deputy_phone or "Not Available" }} <br />
                    </div>
                </div>
            </div>

            <hr style="margin-top: 20px;">

            <div class="row box">
                <h3><span>@lang('messages.graphical_representation')</span></h3>
                <hr style="margin-top: 20px;">

                <div>
                    <span> <h4>@lang('messages.population') : </h4></span>
                    <div class="row">
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfPopulationMaleVSFemale"></canvas>
                        </div>
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfPopulationBodyVSDistrict"></canvas>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 20px;">

                <div>
                    <span> <h4> @lang('messages.total_area') {{ $body->translation()->first()->name }} @lang('messages.in_respect_to_district'): </h4></span>
                    <div class="row">
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfAreaBodyVSDistrict"></canvas>
                        </div>
                        <div class="col-md-6 box">
                            <h4><span>Detail of the Formation of {{ $body->translation()->first()->name }} with old and new wards</span></h4>
                            <p> The contents are not available right now</p>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 20px;">
                <div>
                    <div class="row">
                        <div class="col-md-6">
                            <span> <h4> @lang('messages.population') @lang('messages.of_every') @lang('messages.ward'): </h4></span>
                            <canvas class="box" id="graphOfWardsVsPopulation"></canvas>
                        </div>
                        <div class="col-md-6">
                            <span> <h4> @lang('messages.area') @lang('messages.of_every') @lang('messages.ward'): </h4></span>
                            <canvas class="box" id="graphOfWardsVsArea"></canvas>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 20px;">
                <div>
                    <span> <h4> @lang('messages.other'): </h4></span>
                    <div class="row">
                        <div class="col-md-12">
                            <canvas class="box" id="bar1"></canvas>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <canvas class="box" id="bar2"></canvas>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        {{--@include('app.partials.ad_sense')--}}
    </div>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <h3>Suggest and edit to this Article!!!</h3>
            <hr/>

            @include('app.partials._suggestion_form')

            <input type="hidden" name="type" value="body" />
            <input type="hidden" name="article_id" value="<?php echo $body->id ?>" />

            {!! Form::close() !!}

        </div>

    </div>


    <input type="hidden" id="body_id" value="{{ $body->id }}" />

@endsection

@section('footer-script')
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js') !!}
    {!! HTML::script('js/diagramJS/body_diagrams.js') !!}

    {!! HTML::script('js/suggestion_modal.js') !!}
    <script src='https://www.google.com/recaptcha/api.js'></script>

@endsection