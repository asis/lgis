@extends('app.master')

@section('breadcrumb')
        <!-- BreadCrumb -->
        <div class="container">
                <ol class="breadcrumb">
                        <li><a href="{{ url('/'. App::getLocale().'/province') }}">@lang('messages.province')</a></li>
                        <li><a href="{{ url('/'. App::getLocale().'/district') }}">@lang('messages.district')</a></li>
                        <li class="active">@lang('messages.body')</li>
                </ol>
        </div>
@endsection

@section('content')


        <div class="container">
@php($i = 0)
                @foreach($bodies as $body)

                        <div class="row">
                                <div class="col-md-12 box">
                                        <a href="{{ url('/'. App::getLocale().'/body/'.$body->id) }}">
                                                <h3>{{ $body->translation()->first()->name }} </h3><hr>

                                                <h4>@lang('messages.description'): </h4>
                                                <div class="row">
                                                        <div class="col-md-8">
                                                                <p>
                                                                       {{ $body->translation()->first()->description }}
                                                                </p>

                                                        </div>
                                                        <div class="col-md-4">
                                                                <div class="box img-container">

                                                                        <img src="{{ URL::asset('images/bodies/' . $body->translation('en')->first()->name . '.png')}}" alt=" {{ $body->translation()->first()->name }} " >

                                                                </div>
                                                        </div>
                                                </div>

                                                <hr>
                                        </a>


                                </div>
                        </div>
                        @php($i++)

                @endforeach

                {{ $bodies->links() }}

                @include('app.partials.ad_sense')

        </div>

@endsection

@section('footer-script')
        {!! HTML::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCRaOro7aXDg3MDMKtFj_07Z27nz1TfeQA') !!}
        {!! HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') !!}
        {!! HTML::script('js/bodiesmap.js') !!}
@endsection