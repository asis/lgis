@extends('app.master')

@section('breadcrumb')
    <!-- BreadCrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ url('/'. App::getLocale().'/province') }}">@lang('messages.province')</a></li>
            <li class="active">{{ $province->translation()->first()->name }}</li>
        </ol>
    </div>
@endsection
{{--{{dd($province_info[0])}}--}}
@section('content')
    <div class="container" id="main-container">

        <div>

            <div>
                <div id="titlePosition">
                    <span> <h3> {{ $province->translation()->first()->name  }}  </h3></span>
                </div>

                <div id="editButton">

                    {{--<img src="{{ URL::asset('images/edit.png')}}" alt="Edit" height="32px" width="32px"/>--}}
                    <button id="myBtn" class="edtbtn">
                        <img src="{{ URL::asset('images/edit.png')}}" alt="Edit" height="32px" width="32px"/>
                    </button>

                </div>
            </div>


            <hr>

            <div class="row box">
                <div class="col-md-6">
                    <h4><span>@lang('messages.introduction')</span></h4>
                    <hr>
                    <p>{{  $province->translation()->first()->description }} </p>

                </div>
                <div class="col-md-6">
                    <h4>@lang('messages.map'): </h4>
                    <hr />
                    <div id="map-province" class="box"></div>
                </div>

            </div>

            <hr style="margin-top: 20px;">


            <div class="row box">
                <h3><span>@lang('messages.graphical_representation')</span></h3>

                <hr style="margin-top: 20px;">
                <div>
                    <span> <h4> @lang('messages.population') : </h4></span>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Population of Man Vs Women</h5>
                            <canvas class="box" id="graphOfPopulationMaleVSFemale"></canvas>
                        </div>
                        <div class="col-md-6">
                            <h5>Population of Province Vs Total Population of Nepal</h5>
                            <canvas class="box" id="graphOfPopulationProvinceVSNepal"></canvas>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 20px;">

                <div>
                    <span> <h4> @lang('messages.area'): </h4></span>
                    <div class="row">
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfAreaProvinceVSNepal"></canvas>
                        </div>
                        <div class="col-md-6 box">
                            <h4><span></span></h4>
                                <p>The contents are not available right now.</p>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 20px;">
                <div>
                    <div class="row">
                        <div class="col-md-6">
                            <span> <h4> @lang('messages.population') @lang('messages.of_every') @lang('messages.district'): </h4></span>
                            <canvas class="box" id="graphOfDistrictVsPopulation"></canvas>
                        </div>
                        <div class="col-md-6">
                            <span> <h4> @lang('messages.area') @lang('messages.of_every') @lang('messages.district'): </h4></span>
                            <canvas class="box" id="graphOfDistrictVsArea"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            @include('app.partials.ad_sense')

            <hr style="margin-top: 20px;">
            <div>

{{--                {{dd($district_info)}}--}}
                @php($i=0)
                <span> <h4> Districts : </h4></span>

                <span> <h3> @lang('messages.district') : </h3></span>

                @foreach($province->districts as $district)


                    <div class="row">
                        <div class="col-md-12 box">

                            <a href="{{ url('/'. App::getLocale().'/district/'. $district->id) }}">

                                <h3 id="{{ strtolower($district->translation('en')->first()->name) }}">{{$district->translation()->first()->name  }} </h3>
                                <hr>

                                <h4>@lang('messages.description'): </h4>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>
                                            {{ $district->translation()->first()->description }}
                                        </p>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="box img-container">
                                            <img src="{{ URL::asset('images/districts/' . strtolower($district->translation('en')->first()->name) . '.png') }}" alt=" {{ $district->translation()->first()->name }}">
                                        </div>

                                    </div>

                                </div>
                            </a>
                                {{--Districts: {{ count($district->body) }}--}}
                        </div>
                    </div>
                    @php($i++)
                @endforeach
            </div>

            @include('app.partials.ad_sense')

        </div>
    </div>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <h3>Suggest and edit to this Article!!!</h3>
            <hr/>

            @include('app.partials._suggestion_form')

            <input type="hidden" name="type" value="province" />
            <input type="hidden" name="article_id" value="<?php echo $province->id ?>" />

            {!! Form::close() !!}

        </div>

    </div>

    <input type="hidden" id="province-id" value="<?php echo $province->id ?>" />
    <input type="hidden" id="locale" value="<?php echo App::getLocale() ?>" />

    {{--<script type="text/javascript">--}}
        {{--var provinceid = {!!  $province->translation()->first()->id !!}--}}
        {{--console.log( provinceid);--}}
    {{--</script>--}}

@endsection

@section('footer-script')
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js') !!}
    {!! HTML::script('js/diagramJS/province_diagrams.js') !!}

    {!! HTML::script('js/suggestion_modal.js') !!}
    <script src='https://www.google.com/recaptcha/api.js'></script>

    {!! HTML::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCRaOro7aXDg3MDMKtFj_07Z27nz1TfeQA') !!}
    {!! HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') !!}
    {!! HTML::script('js/provincemap.js') !!}
@endsection