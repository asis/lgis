@extends('app.master')

@section('breadcrumb')
    <!-- BreadCrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li class="active">@lang('messages.province')</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="container">
        @php( $i = 0)
        @foreach($provinces as $province)

            <div class="row">

                <div class="col-md-12 box">
                    <a href="{{ url('/'. App::getLocale().'/province/'.$province->id.'/') }}">
                        <h3> {{ $province->translation()->first()->name }} </h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                @lang('messages.district'): {{ count($province->districts) }}
                                @lang('messages.body'): {{ count($province->bodies) }}
                            </div>
                        </div>
                        <hr>

                        <h4>@lang('messages.description'): </h4>
                        <div class="row">
                            <div class="col-md-8">
                                {{ $province->translation()->first()->description }}
                            </div>
                            <div class="col-md-4">
                                <div class="box img-container">

                                    <img src="{{ URL::asset('images/provinces/province_' . $province->id . '.png')}}" alt=" Province no {{ $province->id }} " >

                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            @php($i++)
        @endforeach

        @include('app.partials.ad_sense')
    </div>
@endsection

    @section('footer-script')

    @endsection
