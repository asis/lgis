<!DOCTYPE html>
<html lang="{{ Session::get('locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>LGIS</title>


    {{--<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">--}}
    {!! HTML::style('//fonts.googleapis.com/css?family=Roboto:300,400,500,700') !!}

    <!-- Bootstrap -->
    {{--<link href="css/bootstrap.min.css" rel="stylesheet">--}}
    {{--<link href="css/style.css" rel="stylesheet">--}}
    {!! HTML::style('css/bootstrap.min.css') !!}
    {!! HTML::style('css/style.css') !!}

    {!! HTML::style('selectize/css/selectize.bootstrap3.css') !!}

@section('head-styles')
    @show

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    {!! HTML::script('//oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') !!}
    {!! HTML::script('//oss.maxcdn.com/respond/1.4.2/respond.min.js') !!}
    <![endif]-->
    {{--<script type="text/javascript"> //<![CDATA[--}}
    {{--var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");--}}
    {{--document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));--}}
    {{--//]]>--}}
    {{--</script>--}}
    @section('head-scripts')
    @show

</head>
<body>

    @include('app.partials._navigation_sliders')

    @include('app.partials._header')

    @yield('breadcrumb')

    @yield('content')

   @include('app.partials._footer')

    <script type="text/javascript">
        var root = '{{url("/")}}';
    </script>

    {!! HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') !!}

    {!! HTML::script('selectize/js/standalone/selectize.min.js') !!}

    {!! HTML::script('js/bootstrap.min.js') !!}
    {!! HTML::script('js/navbar.js') !!}

    {!! HTML::script('js/search.js') !!}

    @section('footer-script')
     @show
</body>
</html>