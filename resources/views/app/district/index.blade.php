@extends('app.master')

@section('breadcrumb')
        <!-- BreadCrumb -->
        <div class="container">
                <ol class="breadcrumb">
                        <li><a href="{{ url('/'.App::getLocale().'/province') }}">@lang('messages.province')</a></li>
                        <li class="active">@lang('messages.district')</li>
                </ol>
        </div>
@endsection

@section('content')


        <div class="container">
                @foreach($districts as $district)

                        <div class="row">
                                <div class="col-md-12 box">
                                        <a href="{{ url('/'. App::getLocale().'/district/'.$district->id) }}">
                                                <h3>{{ $district->translation()->first()->name}} </h3><hr>

                                                <div class="row">
                                                        <div class="col-md-12"> @lang('messages.body'): {{ count($district->bodies) }} </div>
                                                </div>

                                                <hr>

                                                <h4>@lang('messages.description'): </h4>
                                                <div class="row">
                                                        <div class="col-md-8">
                                                                <p>
                                                                        {{ $district->translation()->first()->description  }}
                                                                </p>

                                                        </div>
                                                        <div class="col-md-4">
                                                                <div class="box img-container">
                                                                        <img src="{{ URL::asset('images/districts/' . $district->translation('en')->first()->name . '.png')}}" alt=" {{ $district->translation()->first()->name }}" >
                                                                </div>
                                                        </div>
                                                </div>
                                        </a>
                                </div>
                        </div>
                @endforeach

                {{ $districts->links() }}

        </div>


@endsection

@section('footer-script')
       
@endsection