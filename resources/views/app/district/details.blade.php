@extends('app.master')

@section('breadcrumb')
    <!-- BreadCrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ url('/'. App::getLocale().'/province') }}">@lang('messages.province')</a></li>
            <li><a href="{{ url('/'. App::getLocale().'/province/'. $district->province->id ) }}">{{ $district->province->translation()->first()->name }}</a></li>
            <li><a href="{{ url('/'. App::getLocale().'/district') }}">@lang('messages.district')</a></li>
            <li class="active">{{ $district->translation()->first()->name }}</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="container" id="main-container">

        <div>

            <div>
                <div id="titlePosition">
                    <span> <h3> {{ $district->translation()->first()->name }} </h3></span>
                </div>

                <div id="editButton">

                    {{--<img src="{{ URL::asset('images/edit.png')}}" alt="Edit" height="32px" width="32px"/>--}}
                    <button id="myBtn" class="edtbtn">
                        <img src="{{ URL::asset('images/edit.png')}}" alt="Edit" height="32px" width="32px"/>
                    </button>

                </div>
            </div>

            <hr>

            <div class="row box">
                <div class="col-md-6">
                    <h4><span>@lang('messages.introduction')</span></h4>
                    <hr>
                    <p>{{ $district->translation()->first()->description }}</p>
                </div>
                <div class="col-md-6">
                    <h4>Map: </h4>
                    <hr />
                    <div id="map" class="box"></div>
                </div>

            </div>

            <hr style="margin-top: 20px;">

            <div class="row box">

            {{--@php($allDetail = "")--}}

                {{--@for( $i = 2; $i < count($district_info[0]) ; $i++)--}}
                    {{--@php($allDetail .= $district_info[0][$i] . "--}}
                    {{--" )--}}
                {{--@endfor--}}

                {{--<div class="col-md-12">--}}
                    {{--<p>{{ $allDetail }}</p>--}}
                {{--</div>--}}

            </div>


            <hr style="margin-top: 20px;">

            <div class="row box">
                <h3><span> @lang('messages.graphical_representation') </span></h3>

                <hr style="margin-top: 20px;">
                <div>
                    <span> <h4> @lang('messages.population') : </h4></span>
                    <div class="row">
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfPopulationMaleVSFemale"></canvas>
                        </div>
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfPopulationDistrictVSProvince"></canvas>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 20px;">

                <div>
                    <span> <h4> @lang('messages.area') : </h4></span>
                    <div class="row">
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfAreaDistrictVSProvince"></canvas>
                        </div>
                        <div class="col-md-6 box">
                            <h4><span>Detail of the Formation of {{ $district->translation()->first()->name }} with old and new wards</span></h4>
                            <p> The contents are not available right now</p>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 20px;">
                <div>
                    <span> <h4> Any Other information : </h4></span>
                    <div class="row">
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfBodyVsPopulation"></canvas>
                        </div>
                        <div class="col-md-6">
                            <canvas class="box" id="graphOfBodyVsArea"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <hr style="margin-top: 20px;">
            <div>
                <span> <h3> @lang('messages.body') : </h3></span>
                @php($i =0)
                @foreach($district->bodies as $body)

                    <div class="row">
                        <div class="col-md-12 box">
                            <a href="{{ url('/'.App::getLocale().'/body/'.$body->id) }}">
                                <h3 id = "{{ strtolower( $body->translation('en')->first()->name) }}">{{ $body->translation()->first()->name  }} </h3><hr>

                                <div class="row">
                                    <div class="col-md-12"> @lang('messages.body'): {{ count($district->bodies) }} </div>
                                </div>

                                <hr>

                                <h4>@lang('messages.description'): </h4>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>
                                            {{ $body->translation()->first()->description }}
                                        </p>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="box img-container">

                                            <img src="{{ URL::asset('images/bodies/' . strtolower($body->translation('en')->first()->name) . '.png')}}" alt=" {{ $body->translation()->first()->name }}" >

                                        </div>

                                    </div>
                                </div>

                                <hr>
                            </a>
                        </div>
                    </div>
                    @php($i++)

                @endforeach
            </div>

            @include('app.partials.ad_sense')

        </div>

    </div>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <h3>Suggest and edit to this Article!!!</h3>
            <hr/>

            @include('app.partials._suggestion_form')

            <input type="hidden" name="type" value="district" />
            <input type="hidden" name="article_id" value="<?php echo $district->id ?>" />

            {!! Form::close() !!}

        </div>

    </div>

    <input type="hidden" id="district-name" value="<?php echo $district->translation()->first()->name ?>" />
    <input type="hidden" id="district_id" value="{{ $district->id }}" />

@endsection

@section('footer-script')
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js') !!}
    {!! HTML::script('js/diagramJS/district_diagrams.js') !!}

    {!! HTML::script('js/suggestion_modal.js') !!}
    <script src='https://www.google.com/recaptcha/api.js'></script>

    {!! HTML::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCRaOro7aXDg3MDMKtFj_07Z27nz1TfeQA') !!}
    {!! HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') !!}
    {!! HTML::script('js/districtmap.js') !!}
@endsection