@extends('app.master')

@section('breadcrumb')
    <!-- BreadCrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#" onclick="addProvinces()">@lang('messages.province')</a></li>
        </ol>
    </div>
@endsection

@section('content')

    <div class="container">
        <!--map-->
        <div id="map" class="box"></div>

        <!-- information box -->
        <hr>
        <div class="box">
            <h3 class="text-center" id="name">@lang('messages.nepal')</h3>
            <hr>
            <div class="row">
                <a href="{{ url('/'. App::getLocale().'/body/metro') }}">
                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.metro')</h5>
                        <h2 class="text-center"><span id="mahanagar">{{ $metro }}</span></h2>
                </div>
                </a>

                <a href="{{ url('/'. App::getLocale().'/body/submetro') }}">
                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.sub_metro')</h5>
                        <h2 class="text-center"><span id="upnagar">{{ $sub_metro }}</span></h2>
                </div>
                </a>

                <a href="{{ url('/'. App::getLocale().'/body/munic') }}">
                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.munic')</h5>
                        <h2 class="text-center"><span id="nagar">{{ $muni }}</span></h2>
                </div>
                </a>

                <a href="{{ url('/'. App::getLocale().'/body/ruralmunic') }}">
                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.rural_munic')</h5>
                        <h2 class="text-center"><span id="gau">{{ $rural_muni }}</span></h2>
                </div>
                </a>
            </div>
            <div class="row">
                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.population')</h5>
                        <h2 class="text-center"><span id="popu">2,77,97,457</span></h2>
                </div>

                <a href="{{ url('/'. App::getLocale().'/body/') }}">
                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.body')</h5>
                        <h2 class="text-center"><span id="wards"> {{ $local_bodies }}</span></h2>
                </div>
                </a>

                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.wards')</h5>
                    <h2 class="text-center"><span id="wards">{{ $ward }}</span></h2>
                </div>

                <a href="{{ url('/'. App::getLocale().'/province/') }}">
                <div class="col-md-3 box-light">
                    <h5 class="text-center">@lang('messages.more')</h5>
                        <h2 class="text-center"><span>...</span></h2>
                </div>
                </a>
            </div>
        </div>

        @include('app.partials.ad_sense')

        {{-- Some textual Information about Selected portion--}}
        <hr>
        <div class="box">
            <h3>@lang('messages.introduction')</h3><hr>
            <p>{{ $scrap }}</p>

        </div>

        {{-- Diagrams --}}
        <hr>
        <div>
            <h3 class="text-center">@lang('messages.graphical_representation') of information about Nepal.</h3>

            <div class="row">

                <div class="col-md-6 box">
                    <h5 class="text-center"> Population of Men Vs Women</h5>
                    <canvas id="graphOfPopulationMaleVSFemale"></canvas>
                </div>

                <div class="col-md-6 box">
                    <h5 class="text-center"> Population of different Age group </h5>
                    <canvas id="graphOfPopulationAgeGroup"></canvas>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6 box">
                    <h5 class="text-center"> Population on each Provinces </h5>
                    <canvas id="graphOfProvincePopulation"></canvas>
                </div>

                <div class="col-md-6 box">
                    <h5 class="text-center"> Area of each Provinces in km sq. </h5>
                    <canvas id="graphOfProvinceArea"></canvas>
                </div>

            </div>
        </div>

    </div>

    <input type="hidden" id="locale" value="<?php echo App::getLocale() ?>" />

@endsection

@section('footer-script')
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    {!! HTML::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCRaOro7aXDg3MDMKtFj_07Z27nz1TfeQA') !!}
    {!! HTML::script('js/map.js') !!}

    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js') !!}
    {!! HTML::script('js/diagramJS/nepal_diagram.js') !!}
@endsection