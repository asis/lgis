<!-- This is side bar for menu -->
<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #2874f0; padding-top: 10px;">
        <span class="sidenav-heading"><a href="{{ url('/') }}">Home</a></span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    </div>
    <a data-toggle="collapse" href="#collapse">Detail view</a>

    <div id="collapse" class="panel-collapse collapse">
         <a href="{{ url('/province') }}">Province</a>
        <a href="{{ url('/district') }}">Districts</a>
        <a href="{{ url('/body') }}">Bodies</a>
    </div>

    <a href="{{ url ('/help') }}">Help</a>
    <a href="{{ url ('/about') }}">About us</a>
    <a href="{{ url ('/contact') }}">Contact us</a>

</div>

<!-- Hover -->
<div id="hover" style="font-family:Fontasy Himali"></div>
