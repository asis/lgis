{!! Form::open(array('route' => 'suggestion_store', 'class' => 'form')) !!}

<div class="form-group">
    <label> Your Information</label>
    <div class="row">
        <div class="col-md-6">
            <input type="text" class="form-control" name="name" placeholder="Enter the Name" required>
        </div>

        <div class="col-md-6">
            <input type="text" class="form-control" name="address" placeholder="Enter your Address." required>
        </div>
    </div>
</div>

<div class="form-group">
    <label> Email (*)</label>
    <input type="email" class="form-control" name="email" placeholder="Email" required>
</div>

<div class="form-group">
    <label> Your Information(*)</label>
    <div class="row">
        <div class="col-md-12">
            <textarea class="form-control" name="description" rows="20" placeholder="Suggestions" required></textarea>
        </div>
    </div>
</div>

<div class="g-recaptcha" data-sitekey="6LfpfTUUAAAAAAWqcGSitZ_0w12XfhRcBhiKRtex"></div>

<div class="box-footer">
    <div id="btnClose" class="btn btn-danger">Cancel</div>
    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
</div>
