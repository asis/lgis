<!-- Navigation Bar -->
<div id="lgis-navbar">
    <div class="container">
        <div class="row row1">
            <ul class="largenav pull-right">
                <li class="upper-links dropdown"><a class="links" href="#">@lang('messages.language')</a>
                    <ul class="dropdown-menu">
                        @if( (App::getLocale())==='ne')
                            <li class="profile-li"><a class="profile-links" href="{{ str_replace_first('ne','en',Request::url()) }}">English</a></li>
                            {{--{{ Session::put('locale','en') }}--}}
                            {{--{{ \App::setLocale('en') }}--}}
                        @else
                            <li class="profile-li"><a class="profile-links" href="{{ str_replace_first('en','ne',Request::url()) }}">नेपाली</a></li>
                        @endif
                    </ul>
                </li>
                <li class="upper-links dropdown"><a class="links" href="#">@lang('messages.detail_view')</a>
                    <ul class="dropdown-menu">
                        <li class="profile-li"><a class="profile-links" href="{{ url('/'. App::getLocale().'/province') }}">@lang('messages.province')</a></li>
                        <li class="profile-li"><a class="profile-links" href="{{ url('/'. App::getLocale().'/district') }}">@lang('messages.district')</a></li>
                        <li class="profile-li"><a class="profile-links" href="{{ url('/'. App::getLocale().'/body') }}">@lang('messages.body')</a></li>
                    </ul>
                </li>
                <li class="upper-links"><a class="links" href="{{ url('/'.App::getLocale().'/help') }}">@lang('messages.help')</a></li>
                <li class="upper-links"><a class="links" href="{{ url('/'. App::getLocale().'/about') }}">@lang('messages.about')</a></li>
                <li class="upper-links"><a class="links" href="{{ url('/'. App::getLocale().'/contact') }}">@lang('messages.contact')</a></li>
            </ul>
        </div>
        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ LGIS</span></h2>
                <h1 style="margin:0px;"><span class="largenav"><a href="{{ url ('/'. App::getLocale() ) }}">@lang('messages.name')</a></span></h1>
            </div>
            <div class="lgis-navbar-search smallsearch col-sm-8 col-xs-11">
                <div class="row">

                    {{--<input type="text" id="searchbox" name="q" placeholder="@lang('messages.search')" class="lgis-navbar-input col-xs-11">--}}
                      {{----}}
                    {{--</input>--}}
                    <input class="lgis-navbar-input col-xs-11" placeholder="@lang('messages.search')" id="searchbox" type="text" list="livesearch" />
                    <datalist id="livesearch">
                    </datalist>

                    {{--<input class="lgis-navbar-input col-xs-11" type="" placeholder="@lang('messages.search')" id="typeahead">--}}

                    <button id = "searchbutton" class="lgis-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <!-- Svg of the  Search Image -->
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>

                </div>
            </div>
        </div>
    </div>
</div> <!-- end of nav bar -->

<input type="hidden" id="locale" value="<?php echo App::getLocale() ?>" />