<!-- Footer -->
<div id="footer">
    <div class="container-fluid" style="{width: 100%;}">
        <p class="text-center"> @lang('messages.copyright') © LGIS @lang('messages.year')@lang('messages.fullstop') @lang('messages.all_right_reserved')@lang('messages.fullstop') </p>
        <span class="pull-right">Powered by <a href="#"> OUR COMPANY</a></span>
    </div>
</div>