@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" class="active"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            @php
                $count = 1;
                $break = 2;
            @endphp

            @foreach($suggestions as $suggestion)
                @if( $count == 1)
                    <div class="row">
                        @endif

                        <div class="col-md-6">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title"> Edit suggested on {{ $suggestion->type }} - {{ $suggestion->article_name }}</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <h4>Content: </h4><br/>

                                    {{ str_limit($suggestion->content, $limit = 150, $end = '...') }}

                                    <hr>
                                    Edit send by: {{ $suggestion->name }} <br> Email {{ $suggestion->email }} <br> at {{ $suggestion->created_at }}
                                    <hr>

                                    <h4>Link on App Page:</h4> <a href=" {{ url('en/' . $suggestion->type . '/' . $suggestion->article_id) }}" target="_blank"> {{ url('en/' . $suggestion->type . '/' . $suggestion->article_id) }} </a>
                                    <h4>Link on Admin Edit Page: </h4>  <a href=" {{ url('lgisadminpanel/' . $suggestion->type . '/' . $suggestion->article_id) }}" target="_blank"> {{ url('en/' . $suggestion->type . '/' . $suggestion->article_id) }} </a>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <a href="{{ url('lgisadminpanel/suggestion/'. $suggestion->id) }}">
                                                <button type="button" class="btn btn-block btn-info">View</button>
                                            </a>
                                        </div>

                                        <div class="col-md-4">
                                            <a href="{{ url('lgisadminpanel/suggestion/delete/'. $suggestion->id) }}">
                                                <button type="button" class="btn btn-block btn-danger">Delete</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                        @php $count++ @endphp

                        @if( $count > $break)
                            </div>
                    @php $count = 1 @endphp
                @endif
            @endforeach
            {{ $suggestions->links() }}

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection