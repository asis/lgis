@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Provinces
                <small>Admin-Panel</small>
                {{--<span><a href="{{ url('/admin/province/create') }}"  style="padding-left: 15px;"><i class="fa fa-plus"></i></a></span>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Province</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            @php
                 $count = 1;
                 $break = 2;
            @endphp

                @foreach($provinces as $province)
                    @if( $count == 1)
                        <div class="row">
                    @endif

                            <div class="col-md-6">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <i class="fa fa-text-width"></i>
                                        <h3 class="box-title">{{ $province->translation('en')->first()->name }}</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <dl class="dl-horizontal">
                                            <dt>Description</dt>
                                            <dd>{{ $province->translation('en')->first()->description }}</dd>
                                            <dt>Created On:</dt>
                                            <dd>{{ $province->translation('en')->first()->created_at }}</dd>
                                            <dt>Last Updated on:</dt>
                                            <dd>{{ $province->translation('en')->first()->updated_at }}</dd>
                                        </dl>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <a href="{{ route('province.show', $province->id) }}">
                                                    <button type="button" class="btn btn-block btn-info">View</button>
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="{{ route('province.edit', $province->id) }}">
                                                    <button type="button" class="btn btn-block btn-primary">Edit</button>
                                                </a>
                                            </div>

                                            {{--<div class="col-md-4">--}}
                                                {{--<a href="{{ url('province.delete/',$province->id) }}">--}}
                                                    {{--<button type="button" class="btn btn-block btn-danger">Delete</button>--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>

                    @php $count++ @endphp

                    @if( $count > $break)
                        </div>
                        @php $count = 1 @endphp
                    @endif
                @endforeach

            {{--<div class="row">--}}
                {{--<div class="pull-right" style="padding: 10px;">--}}
                    {{--<span class="info-box-icon bg-aqua"><i class="fa fa-plus"></i></span>--}}
                {{--</div>--}}
            {{--</div>--}}

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection