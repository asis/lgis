@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Province - Create
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('/admin/province') }}"><i class="fa fa-dashboard"></i> Province</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> create</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->

            <div class="row">
                <div class="col-md-6">
                    <p class="text-center">In English</p>
                </div>

                <div class="col-md-6">
                    <p class="text-center">In Nepali</p>
                </div>
            </div>

            {!! Form::open(array('route' => 'province.store','method'=>'POST')) !!}
                @include('admin.partials._form_province_create')
            {!! Form::close() !!}
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection