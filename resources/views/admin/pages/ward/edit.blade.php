@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Wards - Edit
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('/admin/body') }}"><i class="fa fa-dashboard"></i> Wards</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            {!! Form::model($ward, ['method' => 'PATCH','route' => ['ward.update', $ward->id]]) !!}
            @include('admin.partials._form_ward_edit')
            {!! Form::close() !!}
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection