@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ward - Create
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> create</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            {!! Form::open(array('route' => 'ward.store','method'=>'POST')) !!}
                @include('admin.partials._form_ward_create')
            {!! Form::close() !!}
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection