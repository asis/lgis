<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>LGIS | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
{!! HTML::style('css/bootstrap.min.css') !!}

<!-- Font Awesome Icons -->
{!! HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
<!-- Ionicons -->
{!! HTML::style('http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css') !!}
<!-- Theme style -->
{!! HTML::style('css/AdminLTE.min.css') !!}
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
{!! HTML::style('css/skins/skin-blue.min.css') !!}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {!! HTML::script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') !!}
    {!! HTML::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') !!}
    <![endif]-->
</head>
<body class="hold-transition login-page">

    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><b>Admin</b> LGIS</a>
        </div>

        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <p>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ( $errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </p>

            {{--<form action="{{ url('/') }}" method="post">--}}
            {{--{!! Form::open(array('route' => 'lgisadminpanel.login','method'=>'POST')) !!}--}}
            {!! Form::open(['action' => 'AdminController@postLogin']) !!}
                <div class="form-group has-feedback">
                    {!! Form::email('email' ,null, ['required', 'placeholder' => 'Email', 'class' => 'form-control']) !!}
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {!! Form::password('password', ['required', 'placeholder' => 'Password' , 'class' => 'form-control']) !!}
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">

                    <div class="col-xs-4">

                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        {!! Form::button('Sign In',['type' => 'submit', 'class' => 'btn btn-primary btn-block btn-flat']) !!}
                    </div>
                    <div class="col-xs-4">

                    </div>
                    <!-- /.col -->
                </div>
                {!! Form::close() !!}
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

<!-- REQUIRED JS SCRIPTS -->

{!! HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') !!}
{!! HTML::script('js/bootstrap.min.js') !!}
{!! HTML::script('js/app.min.js') !!}

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>