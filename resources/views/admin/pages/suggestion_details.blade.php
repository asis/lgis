@extends('admin.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" class="active"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </ol>
        </section>

        <section class="content">

            <div class="row">

                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Edit suggested on {{ $suggestion->type }} - {{ $suggestion->article_name }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <h4>Content: </h4><br/>

                            {{ $suggestion->content }}

                            <hr>
                            Edit send by: {{ $suggestion->name }} <br> Email {{ $suggestion->email }} <br> at {{ $suggestion->created_at }}
                            <hr>

                            <h4>Link on App Page:</h4> <a href=" {{ url('en/' . $suggestion->type . '/' . $suggestion->article_id) }}" target="_blank"> {{ url('en/' . $suggestion->type . '/' . $suggestion->article_id) }} </a>
                            <h4>Link on Admin Edit Page: </h4>  <a href=" {{ url('lgisadminpanel/' . $suggestion->type . '/' . $suggestion->article_id . '/edit') }}" target="_blank"> {{ url('lgisadminpanel/' . $suggestion->type . '/' . $suggestion->article_id . '/edit') }} </a>

                            <hr>

                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                </div>

                                <div class="col-md-4">
                                    <a href="{{ url('lgisadminpanel/suggestion/delete/'. $suggestion->id) }}">
                                        <button type="button" class="btn btn-block btn-danger">Delete</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection