@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Bodies
                <small>Admin-Panel</small>
                <span>
                    <a href="{{ route('body.create')}}"  style="padding-left: 15px;"><i class="fa fa-plus"></i></a>
                </span>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Bodies</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            @php
                $count = 1;
                $break = 2;
            @endphp

            @foreach($bodies as $body)
                @if( $count == 1)
                    <div class="row">
                        @endif

                        <div class="col-md-6">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <i class="fa fa-text-width"></i>
                                    <h3 class="box-title">{{ $body->translation('en')->first()->name }}</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <dl class="dl-horizontal">
                                        <dt>Description</dt>
                                        <dd>{{ $body->translation('en')->first()->description }}</dd>
                                        <dt>Created On:</dt>
                                        <dd>{{ $body->translation('en')->first()->created_at }}</dd>
                                        <dt>Last Updated on:</dt>
                                        <dd>{{ $body->translation('en')->first()->updated_at }}</dd>
                                    </dl>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <a href="{{ route('body.show', $body->id) }}">
                                                <button type="button" class="btn btn-block btn-info">View</button>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="{{ route('body.edit', $body->id) }}">
                                                <button type="button" class="btn btn-block btn-primary">Edit</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                        @php $count++ @endphp

                        @if( $count > $break)
                    </div>
                    @php $count = 1 @endphp
                @endif
            @endforeach

            {{ $bodies->links() }}

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection