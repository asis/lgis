@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Bodies
                <small>Admin-Panel</small>
                {{--<span><a href="{{ url('/admin/province/create') }}"  style="padding-left: 15px;"><i class="fa fa-plus"></i></a></span>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/lgisadminpannel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{$bodies->translation('en')->first()->name}}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box-body">

                {{ $bodies->translation('en')->first()->name }} <br/>

                {{ $bodies->translation('en')->first()->description  }} <br/>

                {{ $bodies->translation('en')->first()->headquarter  }} <br/>

                {{ $bodies->translation('en')->first()->population  }} <br/>

                {{ $bodies->translation('en')->first()->area  }} <br/>

                {{ $bodies->translation('en')->first()->border  }} <br/>

            </div>

            <div class="row">
                <div class="col-md-4">
                    <a href="{{ route('body.edit', $bodies->id) }}">
                        <button type="button" class="btn btn-block btn-primary">Edit</button>
                    </a>
                </div>

                <div class="col-md-4">
                    <a href="{{ url('lgisadminpanel/body/otherinformation/'. $bodies->id) }}">
                        <button type="button" class="btn btn-block btn-info">Other Information</button>
                    </a>
                </div>
            </div>

        </section><!-- /.content -->
        <section>

            <div class="box-body">
                <h3>List Of Wards<a href="{{ url('lgisadminpanel/ward/create/'. $bodies->id) }}"  style="padding-left: 15px;"><i class="fa fa-plus"></i></a></h3>


            <!-- Your Page Content Here -->
                @php
                    $count = 1;
                    $break = 2;
                @endphp

                @foreach($bodies->ward as $ward)
{{--                    {{dd($ward) }}--}}
                    @if( $count == 1)
                        <div class="row">
                            @endif

                            <div class="col-md-6">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <i class="fa fa-text-width"></i>
                                        <h3 class="box-title">{{ $ward->name }}</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <dl class="dl-horizontal">
                                            <dt>Population</dt>
                                            <dd>{{ $ward->population }}</dd>
                                            <dt>Area</dt>
                                            <dd>{{ $ward->area }}</dd>

                                            <dt>Created On:</dt>
                                            <dd>{{ $ward->created_at }}</dd>
                                            <dt>Last Updated on:</dt>
                                            <dd>{{ $ward->updated_at }}</dd>
                                        </dl>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <a href="{{ route('body.show', $ward->id) }}">
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="{{ route('ward.edit', $ward->id) }}">
                                                    <button type="button" class="btn btn-block btn-primary">Edit</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>

                            @php $count++ @endphp

                            @if( $count > $break)
                        </div>
                        @php $count = 1 @endphp
                    @endif
                @endforeach

            </div>
        </section>

    </div><!-- /.content-wrapper -->

@endsection