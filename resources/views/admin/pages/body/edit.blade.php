@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Bodies - Edit
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('/admin/body') }}"><i class="fa fa-dashboard"></i> Bodies</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->

            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

            <div class="row">
                <div class="col-md-6">
                    <p class="text-center">In English</p>
                </div>

                <div class="col-md-6">
                    <p class="text-center">In Nepali</p>
                </div>
            </div>

            {!! Form::model($body, ['method' => 'PATCH','enctype' => 'multipart/form-data' ,'route' => ['body.update', $body->id]]) !!}
            @include('admin.partials._form_body_edit')
            {!! Form::close() !!}
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection