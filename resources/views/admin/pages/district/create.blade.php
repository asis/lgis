@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Province - Create
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('/admin/province') }}"><i class="fa fa-dashboard"></i> Province</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> create</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->

            <div class="row">
                <div class="col-md-6">
                    <p class="text-center">In English</p>
                </div>

                <div class="col-md-6">
                    <p class="text-center">In Nepali</p>
                </div>
            </div>

            <form role="form">
                <div class="box-body">

                    <div class="form-group">
                        <label> Name</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="name" placeholder="Enter the Name">
                            </div>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="name" placeholder="Name will be Converted in Nepali">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Description</label>
                        <div class="row">
                            <div class="col-md-6">
                                <textarea class="form-control" rows="6" placeholder="Enter the Description in English"></textarea>
                            </div>

                            <div class="col-md-6">
                                <textarea class="form-control" rows="6" placeholder="The description will be converted in Nepali"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Population</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="population" placeholder="Population">
                            </div>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="population" placeholder="Population">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Area(in km<sup>2</sup>)</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="population" placeholder="Area">
                            </div>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="population" placeholder="Area">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Density(per km<sup>2</sup>)</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="population" placeholder="Density">
                            </div>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="population" placeholder="Density">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Border</label>
                        <div class="row">
                            <div class="col-md-6">
                                <h5> East</h5>
                                <input type="text" class="form-control" id="population" placeholder="East Border">
                                <h5> West</h5>
                                <input type="text" class="form-control" id="population" placeholder="West Border">
                                <h5> South</h5>
                                <input type="text" class="form-control" id="population" placeholder="South Border">
                                <h5> North</h5>
                                <input type="text" class="form-control" id="population" placeholder="North Border">
                            </div>

                            <div class="col-md-6">
                                <h5> East</h5>
                                <input type="text" class="form-control" id="population" placeholder="East Border">
                                <h5> West</h5>
                                <input type="text" class="form-control" id="population" placeholder="West Border">
                                <h5> South</h5>
                                <input type="text" class="form-control" id="population" placeholder="South Border">
                                <h5> North</h5>
                                <input type="text" class="form-control" id="population" placeholder="North Border">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Headquaters</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="population" placeholder="Headquater">
                            </div>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="population" placeholder="headquater">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Mix</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="population" placeholder="Mix">
                            </div>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="population" placeholder="Mix">
                            </div>
                        </div>
                    </div>

                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-danger">Cancel</button>
                    <button type="submit" class="btn btn-info">Convert English to Nepali</button>
                    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
                </div>

            </form>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection