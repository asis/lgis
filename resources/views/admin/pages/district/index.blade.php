@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Districts
                <small>Admin-Panel</small>
                {{--<span><a href="{{ url('/admin/district/create') }}"  style="padding-left: 15px;"><i class="fa fa-plus"></i></a></span>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Districts</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            @php
                 $count = 1;
                 $break = 2;
            @endphp

                @foreach($districts as $district)
                    @if( $count == 1)
                        <div class="row">
                    @endif

                            <div class="col-md-6">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <i class="fa fa-text-width"></i>
                                        <h3 class="box-title">{{ $district->translation('en')->first()->name }}</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <dl class="dl-horizontal">
                                            <dt>Description</dt>
                                            <dd>Some Descp.</dd>
                                            <dt>Created On:</dt>
                                            <dd>Random Time</dd>
                                            <dt>Last Updated on:</dt>
                                            <dd>Some Random Time</dd>
                                        </dl>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <a href="{{ route('district.show', $district->id) }}">
                                                    <button type="button" class="btn btn-block btn-info">View</button>
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="{{ route('district.edit', $district->id) }}">
                                                    <button type="button" class="btn btn-block btn-primary">Edit</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>

                    @php $count++ @endphp

                    @if( $count > $break)
                        </div>
                        @php $count = 1 @endphp
                    @endif
                @endforeach

            {{ $districts->links() }}

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection