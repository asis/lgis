@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Districts - Edit
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('/admin/district') }}"><i class="fa fa-dashboard"></i> Districts</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

            <!-- Your Page Content Here -->

            <div class="row">
                <div class="col-md-6">
                    <p class="text-center">In English</p>
                </div>

                <div class="col-md-6">
                    <p class="text-center">In Nepali</p>
                </div>
            </div>

            {!! Form::model($district, ['method' => 'PATCH', 'enctype' => 'multipart/form-data' ,'route' => ['district.update', $district->id] ]) !!}
            @include('admin.partials._form_district_edit')
            {!! Form::close() !!}
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection