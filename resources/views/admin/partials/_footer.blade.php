<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Managed By Our Company
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2017 <a href="{{url('/')}}">LGIS</a>.</strong> All rights reserved.
</footer>