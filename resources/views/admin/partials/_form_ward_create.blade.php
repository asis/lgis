<div class="box-body">

    <div class="form-group">
        <label> Name</label>
        <div class="row">
            <div class="col-md-12">
                <input type="text" required class="form-control" name="name" value="" placeholder="Enter the Name">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Ward ID</label>
        <div class="row">
            <div class="col-md-12">
                <input type="number"  required class="form-control" name="ward_id" value="" placeholder="Enter the ID of ward">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label> Population</label>
        <div class="row">
            <div class="col-md-12">
                <input type="text" required class="form-control" name="population" value="" placeholder="Enter the Population">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label> Area</label>
        <div class="row">
            <div class="col-md-12">
                <input type="text" required class="form-control" name="area" value="" placeholder="Enter the Area">
            </div>
        </div>
    </div>


    <input type="hidden" class="form-control" name="body_id" value="{{ $body_id }}" >

    <div class="box-footer">
        {{--<div class="btn btn-info">Pull data from web</div>--}}
        <button type="submit" class="btn btn-primary pull-right">Save Changes</button>

    </div>
</div>