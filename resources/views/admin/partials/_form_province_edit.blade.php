<div class="box-body">

    <div class="form-group">
        <label> Name</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" required class="form-control" name="name" value="{{$province->translation('en')->first()->name }}" placeholder="Enter the Name">
            </div>

            <div class="col-md-6">
                <input type="text"  required class="form-control" name="name_ne" value="{{ $province->translation('ne')->first()->name }}" placeholder="Name will be Converted in Nepali">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Description</label>
        <div class="row">
            <div class="col-md-6">
                @if(!isset($en_province_info))
                    <textarea class="form-control" required  name="description" rows="6" placeholder="Enter the Description in English">{{ $province->translation('en')->first()->description}}</textarea>
                @else
                    <textarea class="form-control" required  name="description" rows="6" placeholder="Enter the Description in English">{{$en_province_info[0]}}</textarea>
                @endif
            </div>

            <div class="col-md-6">
                @if(!isset($ne_province_info))
                <textarea class="form-control"  required name="description_ne" rows="6"  placeholder="The description will be converted in Nepali">{{ $province->translation('ne')->first()->description}}</textarea>
                @else
                    <textarea class="form-control"  required name="description_ne" rows="6" placeholder="The description will be converted in Nepali">{{$ne_province_info[0]}}</textarea>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Population</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="form-control"  required  value="{{ $province->translation('en')->first()->population }}"  name="population" placeholder="Population">
            </div>

            <div class="col-md-6">
                <input type="text" class="form-control"  required value="{{ $province->translation('ne')->first()->population }}" name="population_ne" placeholder="Population">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Area(in km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="form-control" required  value="{{ $province->translation('en')->first()->area }}"  name="area" placeholder="Area">
            </div>

            <div class="col-md-6">
                <input type="text" class="form-control"  required value="{{ $province->translation('ne')->first()->area }}"  name="area_ne" placeholder="Area">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Density(per km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="form-control" required  value="{{ $province->translation('en')->first()->density }}" name="density" placeholder="Density">
            </div>

            <div class="col-md-6">
                <input type="text" class="form-control" required  value="{{ $province->translation('ne')->first()->density }}"  name="density_ne" placeholder="Density">
            </div>
        </div>
    </div>

    {{--@php--}}

        {{--$border = explode(";", $province->translation('en')->first()->border);--}}

        {{--$east = explode(":",$border[0])[1];--}}
        {{--$west = explode(":",$border[1])[1];--}}
        {{--$south = explode(":",$border[2])[1];--}}
        {{--$north = explode(":",$border[3])[1];--}}

        {{--$border_ne = explode(";", $province->translation('ne')->first()->border);--}}

        {{--$east_ne = explode(":",$border_ne[0])[1];--}}
        {{--$west_ne = explode(":",$border_ne[1])[1];--}}
        {{--$south_ne = explode(":",$border_ne[2])[1];--}}
        {{--$north_ne = explode(":",$border_ne[3])[1];--}}


    {{--@endphp--}}

    {{--<div class="form-group">--}}
        {{--<label> Border</label>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6">--}}
                {{--<h5> East</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $east }}" name="east_border" placeholder="East Border">--}}
                {{--<h5> West</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $west }}" name="west_border" placeholder="West Border">--}}
                {{--<h5> South</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $south }}" name="south_border" placeholder="South Border">--}}
                {{--<h5> North</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $north }}" name="north_border" placeholder="North Border">--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<h5> East</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $east_ne }}" name="east_border_ne" placeholder="East Border">--}}
                {{--<h5> West</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $west_ne }}" name="west_border_ne" placeholder="West Border">--}}
                {{--<h5> South</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $south_ne }}" name="south_border_ne" placeholder="South Border">--}}
                {{--<h5> North</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $north_ne }}" name="north_border_ne" placeholder="North Border">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="form-group">
        <label> Headquarters</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="form-control" required  value="{{ $province->translation('en')->first()->headquarter }}" name="headquarter" placeholder="Headquater">
            </div>

            <div class="col-md-6">
                <input type="text" class="form-control" required  value="{{ $province->translation('ne')->first()->headquarter }}" name="headquarter_ne" placeholder="headquater">
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="btn btn-danger"><a href="{{ route('province.index') }}" style="color: white">Cancel</a></div>
    {{--<div class="btn btn-info">Pull data from web</div>--}}
    <a href="{{ url('lgisadminpanel/province/get_from_web/'.$province->id) }}" class="btn btn-info">Pull data from web</a>
    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>

</div>