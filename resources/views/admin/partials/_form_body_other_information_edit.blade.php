<div class="box-body">

    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <label> Hospital</label>
                <input type="number" class="form-control" name="hospital" value="{{ $information->hospital or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label> Health Center</label>
                <input type="number" class="form-control" name="health_centre" value="{{ $information->health_centre or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label> Health Post</label>
                <input type="number" class="form-control" name="health_post" value="{{ $information->health_post or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label> Sub Health Post</label>
                <input type="number" class="form-control" name="sub_health_post" value="{{ $information->sub_health_post or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label> Ayurvedic Medicine Centre</label>
                <input type="number" class="form-control" name="ayurvedic_medicine_centre" value="{{ $information->ayurvedic_medicine_centre or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label> Rural Health Social Volunteer</label>
                <input type="number" class="form-control" name="rural_health_social_volunteer" value="{{ $information->rural_health_social_volunteer or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>Child Care Centre</label>
                <input type="number" class="form-control" name="child_care_centre" value="{{ $information->child_care_centre or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>Primary School</label>
                <input type="number" class="form-control" name="primary_school" value="{{ $information->primary_school or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>Secondary School</label>
                <input type="number" class="form-control" name="secondary_school" value="{{ $information->secondary_school or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>College</label>
                <input type="number" class="form-control" name="college" value="{{ $information->college or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>University</label>
                <input type="number" class="form-control" name="university" value="{{ $information->university or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>banijya_bank</label>
                <input type="number" class="form-control" name="banijya_bank" value="{{ $information->banijya_bank or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>development_bank</label>
                <input type="number" class="form-control" name="development_bank" value="{{ $information->development_bank or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>financial_institution</label>
                <input type="number" class="form-control" name="financial_institution" value="{{ $information->financial_institution or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>laghu_bittiya_sanstha_ltd</label>
                <input type="number" class="form-control" name="laghu_bittiya_sanstha_ltd" value="{{ $information->laghu_bittiya_sanstha_ltd or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>saving_and_credit_cooperative_ltd</label>
                <input type="number" class="form-control" name="saving_and_credit_cooperative_ltd" value="{{ $information->saving_and_credit_cooperative_ltd or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>pitch_road</label>
                <input type="number" class="form-control" name="pitch_road" value="{{ $information->pitch_road or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>semi_pitch_road</label>
                <input type="number" class="form-control" name="semi_pitch_road" value="{{ $information->semi_pitch_road or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>rough_road</label>
                <input type="number" class="form-control" name="rough_road" value="{{ $information->rough_road or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>driking_water_available</label>
                <input type="number" class="form-control" name="driking_water_available" value="{{ $information->driking_water_available or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>public_toilet_available</label>
                <input type="number" class="form-control" name="public_toilet_available" value="{{ $information->public_toilet_available or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>center_for_media</label>
                <input type="number" class="form-control" name="center_for_media" value="{{ $information->center_for_media or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>micro_hydro</label>
                <input type="number" class="form-control" name="micro_hydro" value="{{ $information->micro_hydro or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>Solar</label>
                <input type="number" class="form-control" name="solar" value="{{ $information->solar or 0 }}" placeholder="">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>bio_gas</label>
                <input type="number" class="form-control" name="bio_gas" value="{{ $information->bio_gas or 0 }}" placeholder="">
            </div>
            <div class="col-md-6">
                <label>improved_stove</label>
                <input type="number" class="form-control" name="improved_stove" value="{{ $information->improved_stove or 0 }}" placeholder="">
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="btn btn-danger"><a href="{{ url('lgisadminpanel/body/'. $body_id) }}" style="color: white">Cancel</a></div>
    {{--<div class="btn btn-info">Pull data from web</div>--}}
    {{--<a href="{{ url('lgisadminpanel/body/get_from_web/'.$body->id) }}" class="btn btn-info">Pull data from web</a>--}}
    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
</div>