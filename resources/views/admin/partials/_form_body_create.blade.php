<div class="box-body">

    <div class="row">

        <div class="col-md-12 dropdown">
            <select name="district_id">
                @foreach($districts as $district)
                    {{--{{ dd($district->id) }}--}}
                    <option value="{{ $district->id }}">{{ $district->translation('en')->first()->name  }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label> Name</label>
        <div class="row">
            <div class="col-md-6">
                <input required type="text" class="form-control" name="name" value="" placeholder="Enter the Name">
            </div>

            <div class="col-md-6">
                <input required  type="text" class="form-control" name="name_ne" value="" placeholder="Name will be Converted in Nepali">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Description</label>
        <div class="row">
            <div class="col-md-6">

                <textarea  required class="form-control" name="description" rows="6" placeholder="Enter the Description in English"></textarea>

            </div>

            <div class="col-md-6">
                <textarea required  class="form-control" name="description_ne" rows="6" placeholder="The description will be converted in Nepali"></textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label> Type</label>
        <div class="row">
            <div class="col-md-12 dropdown">
                <select class="admin-select" name="type">
                    {{--{{ dd($district->id) }}--}}
                    <option value="municipality">municipality</option>
                    <option value="rural_municipality">rural_municipality</option>
                    <option value="sub_metro">sub_metro</option>
                    <option value="metro">metro</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <label> Number of wards</label>
                <input required  type="text" class="form-control"  value=""  name="ward_no" placeholder="Enter number of wards">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label> Map </label>
        <div class="row">
            <div class="col-md-12">
                <input type="file" class="form-control"  value=""  name="map_upload" placeholder="Upload Map">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> People's Representative :- </label>
        <div class="row">
            <div class="col-md-12">
                <label> Mayor:</label><br>
                <label> Name</label>
                <input  required type="text" class="form-control"  value=""  name="mayor_name" placeholder="Enter mayor name">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Email</label>
                <input required  type="text" class="form-control"  value=""  name="mayor_email" placeholder="Enter mayor email">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Phone</label>
                <input required  type="text" class="form-control"  value=""  name="mayor_phone" placeholder="Enter mayor Phone number">
            </div>


        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Photo</label>
                <input type="file" class="form-control"  value=""  name="mayor_photo" placeholder="Enter mayor Phone number">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <label> Deputy Mayor:</label><br>
                <label> Name</label>

                <input  required type="text" class="form-control"  value=""  name="deputy_mayor_name" placeholder="Enter Deputy mayor name">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Email</label>
                <input required  type="text" class="form-control"  value=""  name="deputy_mayor_email" placeholder="Enter deputy mayor Email">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Phone</label>
                <input  required type="text" class="form-control"  value=""  name="deputy_mayor_phone" placeholder="Enter deputy mayor Phone number">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Photo</label>
                <input type="file" class="form-control"  value=""  name="deputy_mayor_photo" placeholder="Enter mayor Phone number">
            </div>

        </div>
    </div>

    <div class="form-group">
        <label> Population</label>
        <div class="row">
            <div class="col-md-12">
                <label> Male</label>
                <input  required type="text" class="form-control"  value=""  name="male_population" placeholder="Population of male">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Female</label>
                <input required  type="text" class="form-control"  value=""  name="female_population" placeholder="Population of female">
            </div>

        </div>

    </div>


    <div class="form-group">
        <label> Area(in km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-12">
                <input  required type="text" class="form-control" value=""  name="area" placeholder="Area">
            </div>

        </div>
    </div>

    <div class="form-group">
        <label> Density(per km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-12">
                <input  required type="text" class="form-control" value="" name="density" placeholder="Density">
            </div>

        </div>
    </div>


    {{--@php--}}

    {{--$border = explode(";", $body->translation('en')->first()->border);--}}

    {{--$east = explode(":",$border[0])[1];--}}
    {{--$west = explode(":",$border[1])[1];--}}
    {{--$south = explode(":",$border[2])[1];--}}
    {{--$north = explode(":",$border[3])[1];--}}

    {{--$border_ne = explode(";", $body->translation('ne')->first()->border);--}}

    {{--$east_ne = explode(":",$border_ne[0])[1];--}}
    {{--$west_ne = explode(":",$border_ne[1])[1];--}}
    {{--$south_ne = explode(":",$border_ne[2])[1];--}}
    {{--$north_ne = explode(":",$border_ne[3])[1];--}}


    {{--@endphp--}}

    {{--<div class="form-group">--}}
    {{--<label> Border</label>--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-6">--}}
    {{--<h5> East</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $east }}" name="east_border" placeholder="East Border">--}}
    {{--<h5> West</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $west }}" name="west_border" placeholder="West Border">--}}
    {{--<h5> South</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $south }}" name="south_border" placeholder="South Border">--}}
    {{--<h5> North</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $north }}" name="north_border" placeholder="North Border">--}}
    {{--</div>--}}

    {{--<div class="col-md-6">--}}
    {{--<h5> East</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $east_ne }}" name="east_border_ne" placeholder="East Border">--}}
    {{--<h5> West</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $west_ne }}" name="west_border_ne" placeholder="West Border">--}}
    {{--<h5> South</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $south_ne }}" name="south_border_ne" placeholder="South Border">--}}
    {{--<h5> North</h5>--}}
    {{--<input type="text" class="form-control" value="{{ $north_ne }}" name="north_border_ne" placeholder="North Border">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    <div class="form-group">
        <label> Headquarters</label>
        <div class="row">
            <div class="col-md-6">
                <input required  type="text" class="form-control" value="" name="headquarter" placeholder="Headquater">
            </div>

            <div class="col-md-6">
                <input  required type="text" class="form-control" value="" name="headquarter_ne" placeholder="headquater">
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="btn btn-danger"><a href="{{ route('body.index') }}" style="color: white">Cancel</a></div>
    {{--<div class="btn btn-info">Pull data from web</div>--}}
    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>

</div>