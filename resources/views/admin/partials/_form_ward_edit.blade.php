<div class="box-body">

    <div class="form-group">
        <label> Name</label>
        <div class="row">
            <div class="col-md-12">
                <input required type="text" class="form-control" name="name" value="{{$ward->name }}" placeholder="Enter the Name">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label> Population</label>
        <div class="row">
            <div class="col-md-12">
                <input required type="text" class="form-control" name="population" value="{{$ward->population }}" placeholder="Enter the Name">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label> Area</label>
        <div class="row">
            <div class="col-md-12">
                <input required type="text" class="form-control" name="area" value="{{$ward->area }}" placeholder="Enter the Name">
            </div>
        </div>
    </div>



<div class="box-footer">
    {{--<div class="btn btn-info">Pull data from web</div>--}}
    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>

</div>
</div>