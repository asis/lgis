<div class="box-body">

    <div class="form-group">
        <label> Name</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text"  required class="form-control" name="name" placeholder="Enter the Name">
            </div>

            <div class="col-md-6">
                <input type="text"  required class="form-control" name="name_ne" placeholder="Name will be Converted in Nepali">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Description</label>
        <div class="row">
            <div class="col-md-6">
                <textarea class="form-control" required  name="description" rows="6" placeholder="Enter the Description in English"></textarea>
            </div>

            <div class="col-md-6">
                <textarea class="form-control" required  name="description_ne" rows="6"  placeholder="The description will be converted in Nepali"></textarea>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Population</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" required  class="form-control"  name="population" placeholder="Population">
            </div>

            <div class="col-md-6">
                <input type="text" required  class="form-control" name="population_ne" placeholder="Population">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Area(in km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text"  required class="form-control" name="area" placeholder="Area">
            </div>

            <div class="col-md-6">
                <input type="text" required  class="form-control" name="area_ne" placeholder="Area">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Density(per km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" required class="form-control" name="density" placeholder="Density">
            </div>

            <div class="col-md-6">
                <input type="text"  required class="form-control" name="density_ne" placeholder="Density">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Border</label>
        <div class="row">
            <div class="col-md-6">
                <h5> East</h5>
                <input type="text"  required class="form-control" placeholder="East Border">
                <h5> West</h5>
                <input type="text"  required class="form-control"  placeholder="West Border">
                <h5> South</h5>
                <input type="text"  required class="form-control" placeholder="South Border">
                <h5> North</h5>
                <input type="text"  required class="form-control" placeholder="North Border">
            </div>

            <div class="col-md-6">
                <h5> East</h5>
                <input type="text"  required class="form-control"  name="east_border_ne" placeholder="East Border">
                <h5> West</h5>
                <input type="text"  required class="form-control"  name="west_border_ne" placeholder="West Border">
                <h5> South</h5>
                <input type="text"  required class="form-control"  name="south_border_ne" placeholder="South Border">
                <h5> North</h5>
                <input type="text"  required class="form-control"  name="north_border_ne" placeholder="North Border">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Headquarters</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text"  required class="form-control"  name="headquarter" placeholder="Headquater">
            </div>

            <div class="col-md-6">
                <input type="text"  required class="form-control"  name="headquarter_ne" placeholder="headquater">
            </div>
        </div>
    </div>

</div>

<div class="box-footer">
    <div class="btn btn-danger"><a href="{{ route('province.index') }}" style="color: white">Cancel</a></div>
    <div class="btn btn-info">Convert English to Nepali</div>
    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
</div>