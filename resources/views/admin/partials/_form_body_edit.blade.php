<div class="box-body">

    <div class="form-group">
        <label> Name</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text"  required class="form-control" name="name" value="{{$body->translation('en')->first()->name }}" placeholder="Enter the Name">
            </div>

            <div class="col-md-6">
                <input type="text"  required class="form-control" name="name_ne" value="{{ $body->translation('ne')->first()->name }}" placeholder="Name will be Converted in Nepali">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Description</label>
        <div class="row">
            <div class="col-md-6">
                @if(!isset($en_body_info))
                    <textarea class="form-control" required  name="description" rows="6" placeholder="Enter the Description in English">{{ $body->translation('en')->first()->description}}</textarea>
                @else
                    <textarea class="form-control" required  name="description" rows="6" placeholder="Enter the Description in English">
                          @if(isset($en_body_info[1]))
                            {{ $en_body_info[1]}}
                        @else
                            {{ "No data found" }}
                        @endif

                    </textarea>
                @endif
            </div>

            <div class="col-md-6">
                @if(!isset($ne_body_info))
                <textarea class="form-control"  required name="description_ne" rows="6"  placeholder="The description will be converted in Nepali">{{ $body->translation('ne')->first()->description}}</textarea>
                @else
                    <textarea class="form-control" required  name="description_ne" rows="6" placeholder="The description will be converted in Nepali">
                        @if(isset($ne_body_info[0]))
                            {{ $ne_body_info[0]}}
                        @else
                            {{ "No data found" }}
                        @endif

                    </textarea>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> Map </label>
        <div class="row">
            <div class="col-md-12">
                <input type="file" class="form-control"  value=""  name="map_upload" placeholder="Upload Map">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label> People's Representative :- </label>
        <div class="row">
            <div class="col-md-12">
                <label> Mayor:</label><br>
                <label> Name</label>
                @if(isset($representaive_name[0]))
                    <input type="text"  required class="form-control"  value="{{ $representaive_name[0] }}"  name="mayor_name" placeholder="Enter mayor name">
                @else
                    <input type="text"  required class="form-control"  value="{{ $body->representative->mayor_name or ""}}"  name="mayor_name" placeholder="Enter mayor name">
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label> Email</label>
            @if(isset($representaive_email[0]))
                    <input type="text"  required class="form-control"  value="{{ $representaive_email[0] }}"  name="mayor_email" placeholder="Enter mayor email">
                @else
                    <input type="text"  required class="form-control"  value="{{ $body->representative->mayor_email  or ""}}"  name="mayor_email" placeholder="Enter mayor email">
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label> Phone</label>
            @if(isset($representaive_phone[0]))
                    <input type="text"  required class="form-control"  value="{{ $representative_phone[0] }}"  name="mayor_phone" placeholder="Enter mayor Phone number">
                @else
                    <input type="text"  required class="form-control"  value="{{ $body->representative->mayor_phone or ""}}"  name="mayor_phone" placeholder="Enter mayor Phone number">
                @endif
            </div>


        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Photo</label>
                {{--@if(isset($body->representative->mayor_pic))--}}
                    {{--<img src="{{ $body->representative->mayor_pic }}" />--}}
                {{--@else--}}
                <input type="file"  class="form-control"  value="{{ $body->representative->mayor_pic or "" }}"  name="mayor_photo" >
                {{--@endif--}}
            </div>
        </div>
        <br>
        <div class="row">
        <div class="col-md-12">
            <label> Deputy Mayor:</label><br>
            <label> Name</label>
            @if(isset($representaive_name[1]))
                <input type="text"  required class="form-control"  value="{{ $representaive_name[1] }}"  name="deputy_mayor_name" placeholder="Enter Deputy mayor name">
            @else
                <input type="text"  required class="form-control"  value="{{ $body->representative->deputy_name or "" }}"  name="deputy_mayor_name" placeholder="Enter Deputy mayor name">
            @endif
        </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Email</label>
                @if(isset($representaive_email[1]))
                    <input type="text"  required class="form-control"  value="{{ $representaive_email[1] }}"  name="deputy_mayor_email" placeholder="Enter deputy mayor Email">
                @else
                    <input type="text"  required class="form-control"  value="{{ $body->representative->deputy_email or "" }}"  name="deputy_mayor_email" placeholder="Enter deputy mayor Email">
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label> Phone</label>
                @if(isset($representative_phone[1]))
                    <input type="text"  required class="form-control"  value="{{ $representative_phone[1] }}"  name="deputy_mayor_phone" placeholder="Enter deputy mayor Phone number">
                @else
                    <input type="text"  required class="form-control"  value="{{ $body->representative->deputy_phone or "" }}"  name="deputy_mayor_phone" placeholder="Enter deputy mayor Phone number">
                @endif
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Photo</label>
                {{--@if(isset($body->representative->deputy_pic))--}}
                    {{--<img src="{{ $body->representative->deputy_pic }}" />--}}
                {{--@else--}}
                <input type="file"  required class="form-control"  value="{{ $body->representative->deputy_pic or "" }}"  name="deputy_mayor_photo" placeholder="Enter mayor Phone number">
                {{--@endif--}}
            </div>

        </div>
    </div>

    <div class="form-group">
        <label> Population</label>
        <div class="row">
            <div class="col-md-12">
                <label> Population </label>
                <input type="text" required class="form-control"  value="{{ $body->translation('en')->first()->population or "0" }}"  name="population" placeholder="Total Population">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Male</label>
                <input type="text" required class="form-control"  value="{{ $body->translation('en')->first()->male_population or "0" }}"  name="male_population" placeholder="Population of male">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <label> Female</label>
                <input type="text" required class="form-control"  value="{{ $body->translation('en')->first()->male_population or "0" }}"  name="female_population" placeholder="Population of female">
            </div>

        </div>

    </div>


    <div class="form-group">
        <label> Area(in km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-12">
                <input type="text"  required class="form-control" value="{{ $body->translation('en')->first()->area }}"  name="area" placeholder="Area">
            </div>

        </div>
    </div>

    <div class="form-group">
        <label> Density(per km<sup>2</sup>)</label>
        <div class="row">
            <div class="col-md-12">
                <input type="text"  required class="form-control" value="{{ $body->translation('en')->first()->density }}" name="density" placeholder="Density">
            </div>

        </div>
    </div>


    {{--@php--}}

        {{--$border = explode(";", $body->translation('en')->first()->border);--}}

        {{--$east = explode(":",$border[0])[1];--}}
        {{--$west = explode(":",$border[1])[1];--}}
        {{--$south = explode(":",$border[2])[1];--}}
        {{--$north = explode(":",$border[3])[1];--}}

        {{--$border_ne = explode(";", $body->translation('ne')->first()->border);--}}

        {{--$east_ne = explode(":",$border_ne[0])[1];--}}
        {{--$west_ne = explode(":",$border_ne[1])[1];--}}
        {{--$south_ne = explode(":",$border_ne[2])[1];--}}
        {{--$north_ne = explode(":",$border_ne[3])[1];--}}


    {{--@endphp--}}

    {{--<div class="form-group">--}}
        {{--<label> Border</label>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6">--}}
                {{--<h5> East</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $east }}" name="east_border" placeholder="East Border">--}}
                {{--<h5> West</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $west }}" name="west_border" placeholder="West Border">--}}
                {{--<h5> South</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $south }}" name="south_border" placeholder="South Border">--}}
                {{--<h5> North</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $north }}" name="north_border" placeholder="North Border">--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<h5> East</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $east_ne }}" name="east_border_ne" placeholder="East Border">--}}
                {{--<h5> West</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $west_ne }}" name="west_border_ne" placeholder="West Border">--}}
                {{--<h5> South</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $south_ne }}" name="south_border_ne" placeholder="South Border">--}}
                {{--<h5> North</h5>--}}
                {{--<input type="text" class="form-control" value="{{ $north_ne }}" name="north_border_ne" placeholder="North Border">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="form-group">
        <label> Headquarters</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text"  required class="form-control" value="{{ $body->translation('en')->first()->headquarter }}" name="headquarter" placeholder="Headquater">
            </div>

            <div class="col-md-6">
                <input type="text"  required class="form-control" value="{{ $body->translation('ne')->first()->headquarter }}" name="headquarter_ne" placeholder="headquater">
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="btn btn-danger"><a href="{{ route('body.index') }}" style="color: white">Cancel</a></div>
    {{--<div class="btn btn-info">Pull data from web</div>--}}
    <a href="{{ url('lgisadminpanel/body/get_from_web/'.$body->id) }}" class="btn btn-info">Pull data from web</a>
    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
</div>